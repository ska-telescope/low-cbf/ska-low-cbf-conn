# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
# pylint: disable=import-error

"""Provides functional health."""

import importlib.resources
from threading import Event, Lock, Thread

import yaml
from ska_control_model import HealthState
from tango import AttrQuality

from .health_attr import HealthAttr


class FunctionPortUp(HealthAttr):
    """Check the percentage of port up ."""

    def __init__(self, name, qual_updater, min_max_failed, min_max_degraded):
        super().__init__(name, qual_updater)
        self.value = 0
        self.min_max_failed = min_max_failed
        self.min_max_degraded = min_max_degraded

    def update_health(self) -> None:
        """Recalculate the health contribution of this attribute based on its
        value and current quality factor."""
        health = HealthState.OK
        if (
            self.value < self.min_max_degraded[0]
            or self.value > self.min_max_degraded[1]
        ):
            health = HealthState.DEGRADED
        if (
            self.value < self.min_max_failed[0]
            or self.value > self.min_max_failed[1]
        ):
            health = HealthState.FAILED
        self.health = health


class FunctionCode(HealthAttr):
    """Check if code is present."""

    def __init__(self, name, qual_updater, fail_state):
        super().__init__(name, qual_updater)
        self.value = True
        self.fail_state = fail_state

    def update_health(self) -> None:
        """Recalculate the health contribution of this attribute based on its
        value and current quality factor."""
        health = HealthState.OK
        if self.value == self.fail_state:
            health = HealthState.FAILED
        self.health = health


class FunctionHealth:
    """
    Periodically monitor indicators of processor's 'functional' health and
    report back via a callback.
    """

    def __init__(self, cfg_file, conn_device, logger):
        """
        Initialisation.

        :param cfg_file: file name containing monitoring params
        :param conn_device: Tango device server
        :param logger: connector logger
        """
        self._conn_device = conn_device
        self._logger = logger
        self._thread = None
        self._lock = Lock()
        self._stop_running = None
        self._last_uptime = 0
        self.health = HealthState.OK
        self._cur_attr_value = {}

        text = importlib.resources.read_text(
            "ska_low_cbf_conn.resources", cfg_file
        )
        self._function_attributes = yaml.load(text, Loader=yaml.Loader) or {}
        for (
            attribute_name,
            attribute_configuration,
        ) in self._function_attributes.items():
            if "port_up" in attribute_name:
                setattr(
                    self,
                    attribute_name,
                    FunctionPortUp(
                        attribute_name,
                        AttrQuality.ATTR_VALID,
                        attribute_configuration["fail_limits"],
                        attribute_configuration["degrade_limits"],
                    ),
                )
            elif "version" in attribute_name:
                setattr(
                    self,
                    attribute_name,
                    FunctionCode(
                        attribute_name,
                        AttrQuality.ATTR_VALID,
                        attribute_configuration["fail_state"],
                    ),
                )

    def start_monitoring(self, check_period: int) -> None:
        """
        Start monitoring thread.

        :param check_period: check period in seconds
        """
        self._thread = Thread(
            target=self._periodic_update, args=(check_period,)
        )
        self._stop_running = Event()
        self._thread.start()

    def stop_monitoring(self) -> None:
        """Stop function health monitoring."""
        self._stop_running.set()
        self._thread.join()

    def _periodic_update(self, check_period: int) -> None:
        """Thread loop periodically checking health indicators."""
        self._logger.info("Start function health monitoring")
        sleep_time = 0  # first run -  check immediately
        while True:
            self._stop_running.wait(sleep_time)
            if self._stop_running.is_set():
                break
            # check all health indicators
            readings = {}
            ports_up = self._conn_device.get_ports_up()
            code = self._conn_device.get_code()
            for attr_name, cfg in self._function_attributes.items():
                if "port_up" in attr_name:
                    readings.update({attr_name: ports_up[attr_name]})
                elif "version" in attr_name:
                    readings.update({attr_name: code[attr_name]})
            if readings:
                self.attr_update(readings)
            sleep_time = check_period

        # clean up when health monitoring is stopped
        self._logger.info("Stopped monitoring function health")
        self._stop_running = None

    def attr_update(self, values: dict = {}) -> None:
        """
        Update function health attributes values.

        :param values: dict key=monitoring point name, value=current reading
        """
        # lock: acessed locally and from Tango attribute (attr_override) change
        with self._lock:
            if not values:
                values = self._cur_attr_value
            something_changed = False
            for name, value in values.items():
                attr = getattr(self, name, None)
                if not attr:
                    self._logger.error(f"Don't have {name}")
                    continue
                # check value change
                if attr.set_value(value):
                    self._cur_attr_value[name] = value
                    something_changed = True

            if something_changed:
                self.summarise_func_health()

    def summarise_func_health(self):
        """Recalculate health_function Tango attribute based on its
        constituent components"""
        health = HealthState.OK
        # add other diagnostic points as we introduce them:
        for attr_name in self._function_attributes.keys():
            if attr := getattr(self, attr_name, None):
                health = max(health, attr.health)
            else:
                self._logger.error(f"Don't have {attr_name} attribute")

        if self.health != health:
            self.health = health
            self._conn_device.update_sub_health("health_function", health)
            self._conn_device.push_change_event("health_function", health)
            self._conn_device.summarise_health_state()
