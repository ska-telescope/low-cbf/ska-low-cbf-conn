# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import logging

from ska_low_cbf_net.connector_protocol import Protocol


class Spead(Protocol):
    def __init__(self, target, gc, bfrt_info):
        # Set up base class
        super(Spead, self).__init__(target, gc)
        self.table = bfrt_info.table_get("spead_table")
        # self.multiplication = bfrt_info.table_get("multiplier_spead")

    def _clear(self):
        """Remove all entries"""
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            self.table.entry_del(
                self.target,
                [
                    self.table.make_key(
                        [
                            self.gc.KeyTuple(
                                "frequency_no",
                                key.to_dict()["frequency_no"]["value"],
                            ),
                            self.gc.KeyTuple(
                                "beam_no", key.to_dict()["beam_no"]["value"]
                            ),
                            self.gc.KeyTuple(
                                "sub_array",
                                key.to_dict()["sub_array"]["value"],
                            ),
                        ]
                    )
                ],
            )

    def clear(self):
        self._clear()

    def add_entry(self, match, action):
        """Add one entry.

        Keyword arguments:
            action -- Dest port
            match -- CODIF Frequency to route on
        """
        frequency = match[0]
        beam = match[1]
        sub_array = match[2]
        self.table.entry_add(
            self.target,
            [
                self.table.make_key(
                    [
                        self.gc.KeyTuple("frequency_no", frequency),
                        self.gc.KeyTuple("beam_no", beam),
                        self.gc.KeyTuple("sub_array", sub_array),
                    ]
                )
            ],
            [
                self.table.make_data(
                    [self.gc.DataTuple("dest_port", action)],
                    "set_egr_port_freq",
                )
            ],
        )

    def remove_entry(self, match):
        """Remove one entry"""
        frequency = match[0]
        beam = match[1]
        sub_array = match[2]
        self.table.entry_del(
            self.target,
            [
                self.table.make_key(
                    [
                        self.gc.KeyTuple("frequency_no", frequency),
                        self.gc.KeyTuple("beam_no", beam),
                        self.gc.KeyTuple("sub_array", sub_array),
                    ]
                )
            ],
        )

    def get_match(self, action):
        """Get frequency number(s) for a given egress port.

        Returns:
            (success flag, frequency number(s) or error message)
        """
        entries = []
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            if data.to_dict()["dest_port"] is action:
                freq_beam_sub_array = "{} {} {}".format(
                    key.to_dict()["frequency_no"]["value"],
                    key.to_dict()["beam_no"]["value"],
                    key.to_dict()["sub_array"]["value"],
                )
                entries.append(freq_beam_sub_array)
        if len(entries) == 0:
            return (False, "Egress port not found")

        return (True, entries)

    def get_action(self, match):
        """Get egress port for a given frequency number.

        Returns:
            (success flag, egress port or error message)
        """
        frequency = match[0]
        beam = match[1]
        sub_array = match[2]
        results = self.table.entry_get(
            self.target,
            [
                self.table.make_key(
                    [
                        self.table.make_key(
                            [self.gc.KeyTuple("frequency_no", frequency)]
                        ),
                        self.table.make_key(
                            [self.gc.KeyTuple("beam_no", beam)]
                        ),
                        self.table.make_key(
                            [self.gc.KeyTuple("sub_array", sub_array)]
                        ),
                    ]
                )
            ],
        )

        return results

    def get_entries(self):
        """Get all forwarding entries.

        Returns:
            list of (frequency_no, dev port)
        """
        entries = {}
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            if "dest_port" in data.to_dict():
                freq_beam_sub_array = "{} {} {}".format(
                    key.to_dict()["frequency_no"]["value"],
                    key.to_dict()["beam_no"]["value"],
                    key.to_dict()["sub_array"]["value"],
                )
                entries[freq_beam_sub_array] = data.to_dict()["dest_port"]

        return entries

    def get_counters(self):
        """Get all forwarding entries.

        Returns:
            list of (match, counters (pkts, bytes))
        """
        entries = {}
        self.table.operations_execute(self.target, "SyncCounters")
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            freq_beam_sub_array = "{} {} {}".format(
                key.to_dict()["frequency_no"]["value"],
                key.to_dict()["beam_no"]["value"],
                key.to_dict()["sub_array"]["value"],
            )
            entries[freq_beam_sub_array] = {
                "Pkts": data.to_dict()["$COUNTER_SPEC_PKTS"],
                "Bytes": data.to_dict()["$COUNTER_SPEC_BYTES"],
            }

        return entries

    def reset_counters(self):
        """Reset SPEAD counters"""

        # Reset direct counter
        self.table.operations_execute(self.target, "SyncCounters")
        resp = self.table.entry_get(self.target, flags={"from_hw": False})

        keys = []
        values = []

        for v, k in resp:
            keys.append(k)

            v = v.to_dict()
            k = k.to_dict()

            values.append(
                self.table.make_data(
                    [
                        self.gc.DataTuple("$COUNTER_SPEC_BYTES", 0),
                        self.gc.DataTuple("$COUNTER_SPEC_PKTS", 0),
                    ],
                    v["action_name"],
                )
            )

        self.table.entry_mod(self.target, keys, values)

    def update_entry(self, match, action):
        """
        Keyword arguments:
            action -- Dest port
            match -- beam number
        """
        installed_action = False
        entries_from_switch = self.table.entry_get(self.target)
        frequency = match[0]
        beam = match[1]
        sub_array = match[2]
        for data, key in entries_from_switch:
            if (
                (key.to_dict()["frequency_no"]["value"] == frequency)
                and (key.to_dict()["beam_no"]["value"] == beam)
                and (key.to_dict()["sub_array"]["value"] == sub_array)
            ):
                installed_action = True
        if installed_action:
            self.table.entry_mod(
                self.target,
                [
                    self.table.make_key(
                        [
                            self.gc.KeyTuple("frequency_no", frequency),
                            self.gc.KeyTuple("beam_no", beam),
                            self.gc.KeyTuple("sub_array", sub_array),
                        ]
                    )
                ],
                [
                    self.table.make_data(
                        [self.gc.DataTuple("dest_port", action)],
                        "set_egr_port_freq",
                    )
                ],
            )
        else:
            self.add_entry(match, action)
