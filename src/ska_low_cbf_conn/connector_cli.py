# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

from ska_low_cbf_net.connector_cli import (
    Cli,
    CommandError,
    front_panel_regex,
    launch_cli,
)

from ska_low_cbf_conn.spead_connector import SpeadConnector


class SpeadCli(Cli):
    """Command-Line Interface with support for SPEAD protocol"""

    def do_show_spead_table(self, line):
        """Show the Spead table."""
        try:
            success, entries = self.ctrl.get_spead_table_entries()
            if not success:
                self.error(entries)
            if len(entries) == 0:
                self.out("No entries")
                return
            entries.sort(key=lambda x: x["port"])

            format_string = (
                "  {Frequency:^11} {Beam:^7} {Sub_array:^11} {port:^7}\n"
            )
            header = {
                "Frequency": "Frequency #",
                "Beam": "Beam #",
                "Sub_array": "Sub_array #",
                "port": "Out Port",
            }

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_show_psr_table(self, line):
        """Show the PSR table."""
        try:
            success, entries = self.ctrl.get_psr_table_entries()
            if not success:
                self.error(entries)
            if len(entries) == 0:
                self.out("No entries")
                return
            entries.sort(key=lambda x: x["Beam"])

            format_string = "  {Beam:^7} {port:^7}\n"
            header = {
                "Beam": "Beam #",
                "port": "Out Port",
            }

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_add_spead_table_entry(self, line):
        """Add SPEAD Table entry. The simple table matches a triple (frequency, beam, sub_array) to an egress port.
        This function creates an entry and takes 4 arguments, first the frequency then, beam, number, and then egress port.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) == 2:
                    frequency = int(ports[0].strip())
                    # beam = int(ports[1].strip())
                    # sub_array = int(ports[2].strip())
                    e_port = ports[1].strip()
                    self.ctrl.add_codif_table_entry(frequency, e_port)
                elif len(ports) == 4:
                    frequency = int(ports[0].strip())
                    beam = int(ports[1].strip())
                    sub_array = int(ports[2].strip())
                    e_port = front_panel_regex.match(ports[3].strip())
                    self.ctrl.add_spead_table_entry(
                        frequency, beam, sub_array, e_port
                    )
                    # to create multicast session and add to it

                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_update_spead_table_entry(self, line):
        """Update SPEAD Table entry. The SPEAD table matches a triple (frequency, beam, sub_array) to an egress port.
        This function creates an entry and takes 4 arguments, first the frequency then, beam, number, and then egress port.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) == 2:
                    frequency = int(ports[0].strip())
                    # beam = int(ports[1].strip())
                    # sub_array = int(ports[2].strip())
                    e_port = ports[1].strip()
                    self.ctrl.add_codif_table_entry(frequency, e_port)
                elif len(ports) == 4:
                    frequency = int(ports[0].strip())
                    beam = int(ports[1].strip())
                    sub_array = int(ports[2].strip())
                    e_port = front_panel_regex.match(ports[3].strip())
                    self.ctrl.update_spead_table_entry(
                        frequency, beam, sub_array, e_port
                    )
                    # to create multicast session and add to it

                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_clear_spead_table(self, line):
        """
        Remove entries in the spead unicast table

        """
        self.ctrl.clear_spead_table()

    def do_add_spead_entries(self, line):
        """
        Add Spead Table entries. Based on a file
        Keyword arguments:
            line -- allocator file name
        This function create a entry and take two arguments, first the frequency then, beam, number, and then egress port.
        """
        file_name = line.strip()
        self.out(file_name)
        file1 = open(file_name, "r")
        Lines = file1.readlines()
        sub = "sub"
        star = "***"
        for line in Lines:
            entries = re.split("\W+", line.strip())
            if len(entries[0].strip()) == 1:
                if len(entries) == 5:
                    frequency = int(entries[0].strip())
                    beam = int(entries[1].strip())
                    sub_array = int(entries[2].strip())
                    e_port = int(entries[3].strip())
                    self.ctrl.add_spead_table_entry(
                        frequency, beam, sub_array, e_port
                    )
                elif len(entries) > 5:
                    frequency = int(entries[0].strip())
                    beam = int(entries[1].strip())
                    sub_array = int(entries[2].strip())
                    e_port = int(entries[3].strip())
                    self.ctrl.add_spead_table_entry(
                        frequency, beam, sub_array, e_port
                    )
                    ports = entries[3 : (len(entries) - 1)]
                    # to create multicast session and add to it
                    # TO BE completed

                print(line.strip())
                print(entries)

    def do_show_spead_counter(self, line):
        """Show Counters from Spead table."""
        try:
            success, entries = self.ctrl.get_spead_table_counters()
            if not success:
                self.error(entries)
                return
            if len(entries) == 0:
                self.out("No entries")
                return

            entries.sort(key=lambda x: x["Frequency"])

            format_string = "  {Frequency:^10} {Beam:^7} {Sub_array:^11} {Pkts:^32} {Bytes:^32}\n"
            header = {
                "Frequency": "Freq #",
                "Beam": "Beam #",
                "Sub_array": "Sub_array #",
                "Pkts": "number of packets",
                "Bytes": "number of bytes",
            }

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_show_psr_counter(self, line):
        """Show Counters from Spead table."""
        try:
            success, entries = self.ctrl.get_psr_table_counters()
            if not success:
                self.error(entries)
                return
            if len(entries) == 0:
                self.out("No entries")
                return

            entries.sort(key=lambda x: x["Frequency"])

            format_string = "  {Beam:^7} {Pkts:^32} {Bytes:^32}\n"
            header = {
                "Beam": "Beam #",
                "Pkts": "number of packets",
                "Bytes": "number of bytes",
            }

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_add_psr_table_entry(self, line):
        """Add PSR Table entry. The PSR table matches a beam number to an egress port.
        This function creates an entry and takes two arguments, first the beam number then egress port.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) == 2:
                    beam = int(ports[0].strip())
                    e_port = front_panel_regex.match(ports[1].strip())
                    self.ctrl.add_psr_table_entry(beam, e_port)

                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_update_psr_table_entry(self, line):
        """Update PSR Table entry. The PSR table matches a beam number to an egress port.
        This function updates an entry and takes two arguments, first the beam number then egress port.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) == 2:
                    beam = int(ports[0].strip())
                    e_port = front_panel_regex.match(ports[1].strip())
                    self.ctrl.update_psr_table_entry(beam, e_port)

                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_add_spead_multicast_table_entry(self, line):
        """Add CBF Table entry. The CBF table matches a triple (frequency, beam, sub_array) to 2 ports (one for beamformer, one for correlator).
        This function create an entry and take two arguments, first the frequency then, beam, number, and then BF     egress port and finally Correlator eggress port.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) == 5:
                    frequency = int(ports[0].strip())
                    beam = int(ports[1].strip())
                    sub_array = int(ports[2].strip())
                    e_port_bf = front_panel_regex.match(ports[3].strip())
                    e_port_corr = front_panel_regex.match(ports[4].strip())
                    self.ctrl.add_spead_correlator_table_entry(
                        frequency, beam, sub_array, e_port_bf, e_port_corr
                    )
                    # to create multicast session and add to it

                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_show_spead_multicast_table(self, line):
        """Show the Spead table."""
        try:
            success, entries = self.ctrl.get_spead_correlator_table_entries()
            if not success:
                self.error(entries)
            if len(entries) == 0:
                self.out("No entries")
                return
            entries.sort(key=lambda x: x["session"])

            format_string = (
                "  {Frequency:^11} {Beam:^7} {Sub_array:^11} {session:^9}\n"
            )
            header = {
                "Frequency": "Frequency #",
                "Beam": "Beam #",
                "Sub_array": "Sub_array #",
                "session": "Session",
            }

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_update_spead_multicast_table_entry(self, line):
        """Update CBF Table entry. The CBF table matches a triple (frequency, beam, sub_array) to 2 ports (one for beamformer, one for correlator).
        This function updates a entry and take two arguments, first the frequency then, beam, number, and then BF
        egress port and finally Correlator egress port.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) == 5:
                    frequency = int(ports[0].strip())
                    beam = int(ports[1].strip())
                    sub_array = int(ports[2].strip())
                    e_port_bf = front_panel_regex.match(ports[3].strip())
                    e_port_corr = front_panel_regex.match(ports[4].strip())
                    self.ctrl.update_spead_correlator_table_entry(
                        frequency, beam, sub_array, e_port_bf, e_port_corr
                    )
                    # to create multicast session and add to it

                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_start_advanced_telemetry(self, line):
        """
        Start advanced telemetry

        """
        try:
            if line:
                ebpf_telemetry = line.split()
                if len(ebpf_telemetry) == 2:
                    interface = ebpf_telemetry[0].strip()
                    interval = float(ebpf_telemetry[1].strip())
                    self.ctrl.start_ebpf_telemetry_table(interface, interval)
                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to front panel name of port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to front panel name of port"
                )
        except CommandError as e:
            self.error(e)

    def do_get_spead_throughput(self, line):
        """Show Counters from Spead table."""
        try:
            entries = self.ctrl.get_spead_throughput()
            msg = "Freq  Sub_Array  Beam       Throughput\n"
            for k, v in entries.items():
                msg = msg + "   {}   {}   {}       {}\n".format(
                    str(k).split()[0], str(k).split()[1], str(k).split()[2], v
                )
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_get_spead_bytes(self, line):
        """Show Counters from Spead table."""
        try:
            entries = self.ctrl.get_spead_bytes()
            msg = "Freq  Sub_Array  Beam       Total_bytes\n"
            for k, v in entries.items():
                msg = msg + "   {}   {}   {}       {}\n".format(
                    str(k).split()[0], str(k).split()[1], str(k).split()[2], v
                )
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_add_ports_to_sdp_arp(self, line):
        """Adding port to the SDP ARP multicast session
        argument is a list of port number using front panel numbering (i.e. 1/0 for port 1 lane 0)
        each port is separated by a space
        """
        list_of_ports_str = line.split(" ")
        list_of_ports = []
        for port in list_of_ports_str:
            port_to_add = front_panel_regex.match(port.strip())
            list_of_ports.append(port_to_add)
        self.ctrl.configure_multicast_for_SDP(list_of_ports)

    def do_add_ip_address_to_sdp_arp(self, line):
        """Adding an IP address to be resolved
        argument is an IP address
        """
        self.ctrl.add_ip_address_to_resolver(line.strip())

    def do_get_arp_results(self, line):
        """Show ARP results from the ARP resolver"""

        try:
            entries = self.ctrl.get_arp_resolver_list()
            msg = "       Ip              Mac            Port\n"
            for k, v in entries.items():
                msg = msg + "   {}   {}         {} \n".format(
                    k, v["mac"], v["port"]
                )
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_add_sdp_ip_table_entry(self, line):
        """Add SDP IP Table entry.
        The simple table matches an IP address to an egress port.
        This function creates an entry and takes 2 arguments, first the IP address, and then egress port.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) == 2:
                    ip_address = ports[0].strip()
                    e_port = e_port_bf = front_panel_regex.match(
                        ports[1].strip()
                    )
                    self.ctrl.add_sdp_ip_table_entry(ip_address, e_port)
                    # to create multicast session and add to it
                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to IP address and an egress port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to IP address and an egress port"
                )
        except CommandError as e:
            self.error(e)

    def do_update_sdp_ip_table_entry(self, line):
        """Update SDP IP Table entry.
        The simple table matches an IP address to an egress port.
        This function creates an entry and takes 2 arguments, first the IP address, and then egress port.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) == 2:
                    ip_address = ports[0].strip()
                    e_port = e_port_bf = front_panel_regex.match(
                        ports[1].strip()
                    )
                    self.ctrl.update_sdp_ip_table_entry(ip_address, e_port)
                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to IP address and an egress port"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to IP address and an egress port"
                )
        except CommandError as e:
            self.error(e)

    def do_clear_sdp_ip_table(self, line):
        """
        Remove entries in the SDP IP table

        """
        self.ctrl.clear_sdp_ip_table()

    def do_show_sdp_ip_counter(self, line):
        """Show Counters from SDP IP Table."""
        try:
            success, entries = self.ctrl.get_sdp_ip_table_counters()
            if not success:
                self.error(entries)
                return
            if len(entries) == 0:
                self.out("No entries")
                return
            self.out(entries)
            entries.sort(key=lambda x: x["IP_Address"])

            format_string = "  {IP_Address:^15} {Pkts:^32} {Bytes:^32}\n"
            header = {
                "IP_Address": "SDP IP Address",
                "Pkts": "number of packets",
                "Bytes": "number of bytes",
            }

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_show_sdp_ip_table(self, line):
        """Show the SDP IP Table."""
        try:
            success, entries = self.ctrl.get_sdp_ip_table_entries()
            if not success:
                self.error(entries)
            if len(entries) == 0:
                self.out("No entries")
                return
            entries.sort(key=lambda x: x["IP_Address"])

            format_string = "  {IP_Address:^15} {port:^7}\n"
            header = {
                "IP_Address": "SDP IP Address",
                "port": "Out Port",
            }

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)

    def do_add_sdp_mac_table_entry(self, line):
        """Add SDP MAC Table entry.
        The SDP MAC table matches an IP address to a MAC address.
        This function creates an entry and takes 2 arguments, first the IP address, and then MAC address.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) == 2:
                    ip_address = ports[0].strip()
                    mac_address = ports[1].strip()
                    self.ctrl.add_sdp_mac_table_entry(ip_address, mac_address)
                    # to create multicast session and add to it
                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to IP address, and MAC address"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to IP address, and MAC address"
                )
        except CommandError as e:
            self.error(e)

    def do_update_sdp_mac_table_entry(self, line):
        """Update SDP MAC Table entry.
        The SDP MAC table matches an IP address to a MAC address.
        This function creates an entry and takes 2 arguments, first the IP address, and then MAC address.
        """
        try:
            if line:
                ports = line.split()
                if len(ports) == 2:
                    ip_address = ports[0].strip()
                    mac_address = ports[1].strip()
                    self.ctrl.update_sdp_mac_table_entry(
                        ip_address, mac_address
                    )
                else:
                    self.error(
                        "Unknown parameter: We need 2 parameters related to IP address, and MAC address"
                    )
            else:
                self.error(
                    "Unknown parameter: We need 2 parameters related to IP address, and MAC address"
                )
        except CommandError as e:
            self.error(e)

    def do_clear_sdp_mac_table(self, line):
        """
        Remove entries in the SDP MAC Table entry

        """
        self.ctrl.clear_sdp_mac_table()

    def do_show_sdp_mac_table(self, line):
        """Show the SDP MAC Table."""
        try:
            success, entries = self.ctrl.get_sdp_mac_table_entries()
            if not success:
                self.error(entries)
            if len(entries) == 0:
                self.out("No entries")
                return
            entries.sort(key=lambda x: x["IP_Address"])

            format_string = "  {IP_Address:^15} {MAC:^15}\n"
            header = {
                "IP_Address": "SDP IP Address",
                "MAC": "SDP MAC Address",
            }

            msg = format_string.format(**header)
            for v in entries:
                msg += format_string.format(**v)
            self.out(msg)

        except CommandError as e:
            self.error(e)


def main():
    launch_cli(SpeadCli, SpeadConnector)


if __name__ == "__main__":
    main()
