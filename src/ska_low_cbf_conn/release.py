# -*- coding: utf-8 -*-
"""Release information for ska_low_cbf_conn"""

name = """ska_low_cbf_conn"""
version = "0.7.2"
version_info = version.split(".")
description = """SKA Low CBF Connector
In-Network Processor control software.
Includes code for Tango control system and SKA-specific protocols."""
author = "CSIRO"
author_email = ""
license = """CSIRO Open Source Licence"""
url = """https://www.skatelescope.org/"""
copyright = """CSIRO"""
