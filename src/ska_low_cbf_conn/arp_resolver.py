# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2022 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import socket
import sys
import time
from threading import Thread

from scapy.all import ARP, UDP, Ether, sendp, sniff


class ARPResolver:
    def __init__(self, interface: str = "enp4s0f0"):
        self.ip_to_mac_and_port = {}
        self.interface = interface

    def add_ip_to_resolve(self, ip_address: str):
        if ip_address not in self.ip_to_mac_and_port:
            self.ip_to_mac_and_port[ip_address] = {
                "mac": "00:00:00:00:00:00",
                "port": 0,
            }

    def remove_ip_to_resolve(self, ip_address: str):
        if ip_address not in self.ip_to_mac_and_port:
            return
        del self.ip_to_mac_and_port[ip_address]

    def send_arp_request(self):
        while True:
            for key, value in list(self.ip_to_mac_and_port.items()):
                sendp(
                    Ether(src="00:11:22:33:44:55", dst="ff:ff:ff:ff:ff:ff")
                    / ARP(
                        op=1,
                        hwsrc="00:11:22:33:44:55",
                        hwdst="ff:ff:ff:ff:ff:ff",
                        psrc="192.168.1.101",
                        pdst=key,
                    )
                    / ("\x00" * 18),
                    iface=self.interface,
                    verbose=0,
                )
            time.sleep(10)

    def receive_arp_reply(self, port: int = 5280):
        while True:
            sniff(
                filter="udp and port {}".format(port),
                iface=self.interface,
                prn=self.custom_action,
                store=False,
            )

    def custom_action(self, packet):
        payload = bytes(packet[UDP].payload)
        ip_address = "{}.{}.{}.{}".format(
            payload[8], payload[9], payload[10], payload[11]
        )
        mac_address = f"{payload[2]:02x}:{payload[3]:02x}:{payload[4]:02x}:{payload[5]:02x}:{payload[6]:02x}:{payload[7]:02x}"
        port = int.from_bytes(payload[:2], "big")
        self.ip_to_mac_and_port[ip_address] = {
            "mac": mac_address,
            "port": port,
        }

    def get_ip_addresses_to_mac_and_port(self):
        return self.ip_to_mac_and_port


def main():
    """Used for testing without running the whole show"""
    resolver = ARPResolver()
    resolver.add_ip_to_resolve("192.168.1.1")
    send_thread = Thread(target=resolver.send_arp_request)
    send_thread.start()
    resolver_thread = Thread(target=resolver.receive_arp_reply)
    resolver_thread.start()


if __name__ == "__main__":
    main()
