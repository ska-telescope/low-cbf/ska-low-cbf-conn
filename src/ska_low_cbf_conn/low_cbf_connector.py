# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

""" In-Network Processor Control

The LowCbfConnector Tango Device provides control of the routing tables
of the SKA Low CBF in-network processors.
"""

import json
import os
from copy import deepcopy
from subprocess import STDOUT, check_call, check_output
from threading import Thread
from time import sleep
from typing import Any, Iterable

# PyTango imports
import tango
from ska_low_cbf_net.connector_cli import CommandError, front_panel_regex
from ska_tango_base import SKABaseDevice
from ska_tango_base.commands import FastCommand, ResultCode
from ska_tango_base.control_model import HealthState, TestMode
from tango import AttReqType, AttributeProxy, AttrWriteType
from tango.server import attribute, command, device_property, run

import ska_low_cbf_conn.release as release
from ska_low_cbf_conn.connector_component_manager import (
    ConnectorComponentManager,
)
from ska_low_cbf_conn.connector_port_monitor import SwitchMonitor
from ska_low_cbf_conn.default_connections import (
    default_connection_list,
    initialise_list_ports,
)
from ska_low_cbf_conn.health_function import FunctionHealth
from ska_low_cbf_conn.health_hardware import HardwareHealth
from ska_low_cbf_conn.health_process import ProcessHealth
from ska_low_cbf_conn.spead_connector import SpeadConnector

NUMBER_OF_PHYSICAL_PORTS = 32
HW_HEALTH_CHECK_PERIOD = 15
FUNC_HEALTH_CHECK_PERIOD = 5
PROC_HEALTH_CHECK_PERIOD = 5
HW_HEALTH_CONFIG_FILE = "health_hardware.yaml"
FUNC_HEALTH_CONFIG_FILE = "health_function.yaml"
PROC_HEALTH_CONFIG_FILE = "health_process.yaml"


__all__ = ["LowCbfConnector", "main"]


def overridable(func):
    """Decorator to apply test mode overrides to attributes."""
    attr_name = func.__name__

    def override_attr_in_test_mode(self, *args, **kwargs):
        """Override attribute when test mode is active and value specified."""
        if (
            self._test_mode == TestMode.TEST
            and attr_name in self._attr_overrides
        ):
            return _override_value_convert(
                attr_name, self._attr_overrides[attr_name]
            )

        # Test Mode not active, normal attribute behaviour
        return func(self, *args, **kwargs)

    return override_attr_in_test_mode


def _override_value_convert(attr_name: str, value: Any) -> Any:
    """Automatically convert types for attr overrides (e.g. enum label -> int)."""
    enum_attrs = {
        "healthState": HealthState,
        "health_hardware": HealthState,
        "health_process": HealthState,
        "health_function": HealthState,
    }
    """Map attribute name to enum class"""
    if attr_name in enum_attrs and type(value) is str:
        return enum_attrs[attr_name][value]

    # default to no conversion
    return value


class LowCbfConnector(SKABaseDevice):
    """
    The LowCbfConnector Tango Device provides control of the routing tables
    of the SKA Low CBF in-network processors.

    **Properties:**

    - Device Property
        Address
            - IP address to the gRPC interface
            - Type:'DevString'
    """

    # -----------------
    # Device Properties
    # -----------------

    bfrt_address = device_property(
        dtype="DevString",
        mandatory=True,
        doc="BareFoot Runtime gRPC address and port",
    )

    bfrt_shell_address = device_property(
        dtype="DevString",
        mandatory=True,
        doc="BareFoot shell gRPC address and port",
    )

    allocator_address = device_property(
        dtype="DevString",
        mandatory=True,
        doc="Allocator Device FQDN",
    )
    ports_configuration = device_property(
        dtype="DevString",
        mandatory=True,
        doc="Port configuration from helm chart",
    )
    switch = device_property(
        dtype="DevString",
        mandatory=True,
        doc="Switch name",
    )
    ebpf_configuration = device_property(
        dtype="DevString",
        mandatory=True,
        doc="eBPF configuration for the advanced telemetry as net_interface:report_interval",
    )
    hardware_connections = device_property(
        dtype=("DevString",),
        mandatory=False,  # if not present, reads test_connections.py
        doc="List of P4 switch to Alveo and I/O hardware cabling",
    )

    # ----------
    # Attributes
    # ----------

    # We override ska_tango_base testMode attribute to flush our value overrides
    # when leaving test mode.
    @SKABaseDevice.testMode.write
    def testMode(self: SKABaseDevice, value: TestMode) -> None:
        """
        Set device Test Mode.

        Resets our test mode override values when leaving test mode.
        """
        if value == TestMode.NONE:
            overrides_being_removed = list(self._attr_overrides.keys())
            self._attr_overrides = {}
            self._push_events_overrides_removed(overrides_being_removed)
            self.summarise_health_state()

        self._test_mode = value

    def _push_events_overrides_removed(
        self, attrs_to_refresh: Iterable[str]
    ) -> None:
        """
        Push true value events for attributes that were previously overridden.

        :param attrs_to_refresh: Names of our attributes that are no longer overridden
        """
        for attr_name in attrs_to_refresh:
            # Read configuration of attribute
            attr_cfg = self.get_device_attr().get_attr_by_name(attr_name)
            manual_event = (
                attr_cfg.is_change_event() or attr_cfg.is_archive_event()
            )

            if not manual_event:
                continue

            # Read current state of attribute
            attr = AttributeProxy(f"{self.get_name()}/{attr_name}").read()
            if attr_cfg.is_change_event():
                self.push_change_event(attr_name, attr.value)
                # FIXME - replace above call with the below
                #  - needs latest ska-tango-base
                # push_change_event(attr_name, attr.value, attr.time, attr.quality)
            if attr_cfg.is_archive_event():
                self.push_archive_event(attr_name, attr.value)
                # FIXME - replace above call with the below
                #  - needs latest ska-tango-base
                # push_archive_event(attr_name, attr.value, attr.time, attr.quality)

    @attribute(
        dtype=str,
        doc="Attribute value overrides (JSON dict)",
    )
    def test_mode_overrides(self: SKABaseDevice) -> str:
        """
        Read the current override configuration.

        :return: JSON-encoded dictionary (attribute name: value)
        """
        return json.dumps(self._attr_overrides)

    # TODO @test_mode_overrides.is_allowed looks nice, but doesn't work
    #  - maybe needs newer pytango?
    def is_test_mode_overrides_allowed(self, request_type: AttReqType) -> bool:
        """
        Control access to test_mode_overrides attribute.

        Writes to the attribute are allowed only if test mode is active.
        """
        if request_type == AttReqType.READ_REQ:
            return True
        return self._test_mode == TestMode.TEST

    @test_mode_overrides.write  # type: ignore[no-redef]
    def test_mode_overrides(self: SKABaseDevice, value_str: str) -> None:
        """
        Write new override configuration.

        :param value_str: JSON-encoded dict of overrides (attribute name: value)
        """
        value_dict = json.loads(value_str)
        assert isinstance(value_dict, dict), "expected JSON-encoded dict"
        overrides_being_removed = (
            self._attr_overrides.keys() - value_dict.keys()
        )
        self._attr_overrides = value_dict
        self._push_events_overrides_removed(overrides_being_removed)

        for attr_name, value in value_dict.items():
            value = _override_value_convert(attr_name, value)
            # it seems OK to call push_ even if we haven't set_change_event ¯\_(ツ)_/¯
            self.push_change_event(attr_name, value)
            self.push_archive_event(attr_name, value)

        # re-calculate health so overrides take effect immediately
        self.summarise_health_state()

    @attribute(
        dtype="DevString",
        label="SPEAD Unicast Routing Table",
        doc="JSON String encoding the current routing configuration",
    )
    def speadUnicastRoutingTable(self) -> str:
        """Return the routingTable attribute.

        :return: JSON string
        """
        success, table = self.connector.get_spead_table_entries()
        routing_table = {}
        if len(table) > 0:
            routing_table["Spead"] = table
        else:
            routing_table["Spead"] = [
                {"Frequency": 0, "Beam": 0, "Sub_array": 0, "port": "0/0"}
            ]
        self._spead_unicast_routing_table = json.dumps(routing_table)
        return self._spead_unicast_routing_table

    @attribute(
        dtype="DevString",
        label="PSR Routing Table",
        doc="JSON String encoding the current PSR routing configuration",
    )
    def psrRoutingTable(self):
        """Return the routingTable attribute."""
        success, table = self.connector.get_psr_table_entries()
        routing_table = {}
        if len(table) > 0:
            routing_table["PSR"] = table
        else:
            routing_table["PSR"] = [{"Beam": 0, "port": "0/0", "UDP_port": 0}]
        self._psr_routing_table = json.dumps(routing_table)
        return self._psr_routing_table

    @attribute(
        dtype="DevString",
        label="SPEAD Multiplier Routing Table",
        doc="JSON String encoding the current SPEAD Multiplier routing configuration",
    )
    def speadMultiplierRoutingTable(self):
        """Return the routingTable attribute."""
        success, table = self.connector.get_spead_multiplier_table_entries()
        routing_table = {}
        if len(table) > 0:
            routing_table["Spead"] = table
        else:
            routing_table["Spead"] = [
                {"Frequency": 0, "Beam": 0, "Sub_array": 0, "session": 0}
            ]
        self._spead_multiplier_routing_table = json.dumps(routing_table)
        return self._spead_multiplier_routing_table

    @attribute(
        dtype="DevString",
        label="ARP Routing Table",
        doc="JSON String encoding the current ARP routing configuration",
    )
    def arpRoutingTable(self):
        """Return the routingTable attribute."""
        success, table = self.connector.get_arp_table_entries()
        routing_table = {}
        if len(table) > 0:
            routing_table["ARP"] = table
        else:
            routing_table["ARP"] = [
                {"IP": "0.0.0.0", "MAC": "00:00:00:00:00:00"}
            ]
        self._arp_routing_table = json.dumps(routing_table)
        return self._arp_routing_table

    @attribute(
        dtype="DevString",
        label="Basic Routing Table",
        doc="JSON String encoding the current Basic routing configuration",
    )
    def basicRoutingTable(self):
        """Return the routingTable attribute."""
        success, table = self.connector.get_simple_table_entries()
        routing_table = {}
        if len(table) > 0:
            routing_table["Basic"] = table
        else:
            routing_table["Basic"] = [{"ingress port": "0/0", "port": "0/0"}]
        self._basic_routing_table = json.dumps(routing_table)
        return self._basic_routing_table

    @attribute(
        dtype="DevString",
        label="SDP IP Routing Table",
        doc="JSON String encoding the current SDP IP routing configuration",
    )
    def sdpIpRoutingTable(self):
        """Return the routingTable attribute."""
        success, table = self.connector.get_sdp_ip_table_entries()
        routing_table = {}
        if len(table) > 0:
            routing_table["SDP_IP"] = table
        else:
            routing_table["SDP_IP"] = [
                {"IP_Address": "0.0.0.0", "port": "0/0"}
            ]
        self._sdp_ip_table = json.dumps(routing_table)
        return self._sdp_ip_table

    @attribute(
        dtype="DevString",
        label="SDP MAC Routing Table",
        doc="JSON String encoding the current SDP MAC routing configuration",
    )
    def sdpMacRoutingTable(self):
        """Return the routingTable attribute."""
        success, table = self.connector.get_sdp_mac_table_entries()
        routing_table = {}
        if len(table) > 0:
            routing_table["SDP_MAC"] = table
        else:
            routing_table["SDP_MAC"] = [
                {"IP_Address": "0.0.0.0", "MAC": "00:00:00:00:00:00"}
            ]
        self._sdp_mac_table = json.dumps(routing_table)
        return self._sdp_mac_table

    @attribute(
        dtype="DevString",
        label="Substation Routing Table",
        doc="JSON String encoding the current SDP MAC routing configuration",
    )
    def subStationTable(self):
        """Return the routingTable attribute."""
        success, table = self.connector.get_sub_station_table_entries()
        if success:
            routing_table = {}
            routing_table["Sub_Station"] = table
            self._sub_station_table = json.dumps(routing_table)
            return self._sub_station_table
        return """" '{"Sub_Station": []}' """

    @attribute(
        dtype="DevString",
        label="Packet Count",
        doc="per physical port and protocol",
    )
    def speadUnicastCounterTotal(self):
        """Return the packetsTotal attribute."""
        success, table = self.connector.get_spead_table_counters()
        routing_table = {}
        routing_table["SPEAD Unicast"] = table
        return json.dumps(routing_table)

    @attribute(
        dtype="DevString",
        label="Total Data",
        unit="B",
        doc="per physical port and protocol",
    )
    def speadMultiplierCounterTotal(self):
        """Return the packetsTotal attribute."""
        success, table = self.connector.get_spead_multiplier_table_counters()
        routing_table = {}
        routing_table["SPEAD Multiplier"] = table
        return json.dumps(routing_table)

    @attribute(
        dtype="DevDouble",
        label="Packet Rate",
        unit="/s",
    )
    def packetRate(self):
        """Return the packetRate attribute."""
        return self._packet_rate

    @attribute(
        dtype="DevString",
        doc="JSON string with current active traffic in bytes",
    )
    def byteRate(self):
        """Return the byteRate attribute."""
        # return self._byte_rate
        return json.dumps(self.connector.get_spead_bytes())

    @attribute(
        dtype="DevString",
        label="Lost Packets",
        doc="SPEAD packets lost per frequency channel",
    )
    def packetsLost(self):
        """Return the packetsLost attribute."""
        return json.dumps(self.connector.lost_spead_packets)

    @attribute(
        dtype="DevString",
        label="Packet Loss Rate",
        doc="SPEAD packets lost as a proportion of all SPEAD packets (lost+received)",
    )
    def packetLossRate(self):
        """Return the packetLossRate attribute."""
        return json.dumps(self.connector.loss_rate_spead_packets)

    @attribute(
        dtype="DevString",
    )
    def staticConfig(self):
        """Return the staticConfig attribute."""
        return self._static_config

    @attribute(
        dtype="DevULong",
    )
    def dynamicRoutes(self):
        """Return the dynamicRoutes attribute."""
        return self._dynamic_routes

    @attribute(
        dtype="DevString",
    )
    def routeCounters(self):
        """Return the routeCounters attribute."""
        return self._route_counters

    @attribute(
        dtype="DevString",
        label="Lost Data",
        unit="B",
        doc="SPEAD packets lost per frequency channel",
    )
    def bytesLost(self):
        """Return the bytesLost attribute."""
        return json.dumps(self.connector.lost_spead_bytes)

    @attribute(
        dtype="DevString",
        label="Data Loss Rate",
        unit="B/s",
        doc="SPEAD bytes lost as a proportion of all SPEAD bytes (lost+received)",
    )
    def bytesLossRate(self):
        """Return the bytesLossRate attribute."""
        return json.dumps(self.connector.loss_rate_spead_bytes)

    @attribute(
        dtype="DevString",
        label="Port Status on the switch",
        doc="Various information related to the active ports on the switch",
    )
    def portStatus(self):
        """Return the portStatus attribute."""
        success, table = self.connector.get_port_statistics()
        routing_table = {}
        if success:
            routing_table["Ports_Status"] = table
        else:
            routing_table["Ports_Status"] = []
        self._port_status = json.dumps(routing_table)
        return self._port_status

    @attribute(
        dtype="DevString",
        label="SDP IP address and their associated mac and port",
        doc="Json string containing IP@ to MAC@ and port for SDP routing",
    )
    def ipAddressToMacAndPort(self):
        """Return the portStatus attribute."""
        table = self.connector.get_arp_resolver_list()
        routing_table = {}
        routing_table["SDP"] = table
        self._ip_address_to_mac_and_port = json.dumps(routing_table)
        return self._ip_address_to_mac_and_port

    @attribute(
        dtype="DevString",
        label="Multicast sessions configured on the switch",
        doc="Json string containing multicast sessions configured on the switch",
    )
    def multicastSessions(self):
        """
        Return the multicast session configured on the switch
        """
        table = self.connector.get_multicast_configurations()
        routing_table = {}
        routing_table["multicast_routing_table"] = table
        self._multicast_session = json.dumps(routing_table)
        return self._multicast_session

    @attribute(dtype=str, memorized=False, hw_memorized=False)
    def arp_replies(self):
        """
        Get route info needed by P4 switches to route SPS packets

        :return: JSON string
        """
        arp_replies = self.connector.get_arp_resolver_list()
        return json.dumps(arp_replies)

    @attribute(dtype=str)
    def health_status(self):
        """
        Get health status from the P4 switch.

        :return: JSON string
        """
        if self._health_status == {}:
            self._health_status = deepcopy(self.connector.get_health_status())
        return json.dumps(self._health_status)

    @attribute(dtype=str)
    def port_rx_throughput(self):
        """
        Get incoming throughput for all ports.

        :return: JSON string
        """
        if self._rx_throughput == {}:
            self._rx_throughput = deepcopy(self.connector.get_health_status())
        return json.dumps(self._rx_throughput)

    @attribute(dtype=str)
    def port_tx_throughput(self):
        """
        Get outgoing throughput for all ports.

        :return: JSON string
        """
        if self._tx_throughput == {}:
            self._tx_throughput = deepcopy(self.connector.get_health_status())
        return json.dumps(self._tx_throughput)

    @attribute(dtype=str)
    def port_rx_pps(self):
        """
        Get incoming throughput in packets per seconds for all ports.

        :return: JSON string
        """
        if self._rx_pps == {}:
            self._rx_pps = deepcopy(self.connector.get_health_status())
        return json.dumps(self._rx_pps)

    @attribute(dtype=str)
    def port_tx_pps(self):
        """
        Get outgoing throughput in packets per seconds for all ports.

        :return: JSON string
        """
        if self._tx_pps == {}:
            self._tx_pps = deepcopy(self.connector.get_health_status())
        return json.dumps(self._tx_pps)

    @attribute(dtype=HealthState, access=AttrWriteType.READ_WRITE)
    @overridable
    def healthState(self) -> HealthState:
        return self._health_state

    @attribute(dtype=str)
    def bfrtAddress(self):
        """
        Return BFRT gRPC address.

        :return: JSON string
        """
        if self._bfrt_address == "":
            self._bfrt_address = "0.0.0.0:1000"
        return json.dumps({"BFRT_Server": self._bfrt_address})

    @attribute(dtype=str)
    def allocatorAddress(self):
        """
        Return Allocator Device FQDN.

        :return: JSON string
        """
        if self._allocator_address == "":
            self._allocator_address = "Allocator_Not_Available"
        return json.dumps({"Allocator": self._allocator_address})

    @attribute(dtype=str)
    def switchName(self):
        """
        Return Switch Name.

        :return: JSON string
        """
        if self._name == "":
            self._name = "Name_not_configured"
        return json.dumps({"Name": self._name})

    @attribute(dtype=str, doc="JSON string with current active traffic")
    def spsRates(self):
        """
        Return all the rates of current active SPS traffic arriving at that switch.

        :return: JSON string with current active traffic
        """
        return json.dumps(self.connector.get_spead_throughput())

    @attribute(dtype=str)
    def unit_serial(self):
        """
        Get serial number of the switch.

        :return: string
        """
        return self._serial_number

    @attribute(dtype=float)
    def hardware_motherboard_temperature(self):
        """
        Get board average temperature.

        :return: average board temperature
        """
        return self._average_temperature

    @attribute(dtype=float)
    def hardware_tofino_temperature(self):
        """
        Get board tofino temperature.

        :return: tofino board temperature
        """
        return self._tofino_temperature

    @attribute(dtype=bool)
    def function_p4_code_version_present(self):
        """
        Indicate if P4 is present.

        :return: true if program is present
        """
        return self._p4_code_version_present

    @attribute(dtype=bool)
    def function_valid_routing_port(self):
        """
        Indicate if requested is valid.

        :return: true if all routes are valid
        """
        return self._valid_routing_port

    @attribute(dtype=float)
    def function_sdp_port_up(self):
        """
        Indicate the percentage of SDP port up.

        :return: percentage of SDP port up
        """
        return self._sdp_port_up

    @attribute(dtype=float)
    def function_alveo_port_up(self):
        """
        Indicate the percentage of Alveo port up.

        :return: percentage of Alveo port up
        """
        return self._alveo_port_up

    @attribute(dtype=float)
    def function_sps_port_up(self):
        """
        Indicate the percentage of SPS port up.

        :return: percentage of SPS port up
        """
        return self._sps_port_up

    @attribute(dtype=float)
    def function_pss_port_up(self):
        """
        Indicate the percentage of PSS port up.

        :return: percentage of PSS port up
        """
        return self._pss_port_up

    @attribute(dtype=float)
    def function_pst_port_up(self):
        """
        Indicate the percentage of PST port up.

        :return: percentage of PST port up
        """
        return self._pst_port_up

    @attribute(dtype=HealthState, doc="hardware health")
    @overridable
    def health_hardware(self) -> HealthState:
        """Summary of hardware health components"""
        return self._health_hardware

    @attribute(dtype=HealthState, doc="function health")
    @overridable
    def health_function(self) -> HealthState:
        """Summary of function health components"""
        return self._health_function

    @attribute(dtype=HealthState, doc="process health")
    @overridable
    def health_process(self) -> HealthState:
        """Summary of process health components"""
        return self._health_process

    def write_healthState(self, value: HealthState) -> None:
        """Update the connector health state"""
        if self._test_mode == TestMode.TEST:
            # case-insensitive override check
            value = self._attr_overrides.get("healthstate", value)
            value = self._attr_overrides.get("healthState", value)

        if value != self._health_state:
            self._health_state = value
            self.logger.info(f"pushing healthState {value}")
            self.push_change_event("healthState", value)
            self.push_archive_event("healthState", value)

    def summarise_health_state(self):
        """Recalculate overall health based on three health categories"""
        health = max(
            self._get_override_value("health_hardware", self._health_hardware),
            self._get_override_value("health_function", self._health_function),
            self._get_override_value("health_process", self._health_process),
        )
        if health != self.healthState:
            self.write_healthState(health)

    def update_sub_health(self, name: str, health_state: HealthState):
        """Update the value of a sub health"""
        if name == "health_hardware":
            self._health_hardware = health_state
            return
        if name == "health_function":
            self._health_function = health_state
            return
        if name == "health_process":
            self._health_process = health_state
            return

    # ---------------
    # General methods
    # ---------------

    def create_component_manager(self):
        return ConnectorComponentManager(
            logger=self.logger,
            communication_state_callback=self._communication_state_changed,
            component_state_callback=self._component_state_changed,
        )

    def _get_override_value(self, attr_name: str, default=None):
        """
        Read a value from our overrides, use a default value when not overridden.

        Used where we use possibly-overridden internal values within the device server
        (i.e. reading member variables, not via the Tango attribute read mechanism).

        e.g.
        ``my_thing = self._get_override_value("thing", self._my_thing_true_value)``
        """
        if (
            self._test_mode != TestMode.TEST
            or attr_name not in self._attr_overrides
        ):
            return default
        return _override_value_convert(
            attr_name, self._attr_overrides[attr_name]
        )

    # ------------------
    # Attributes methods
    # ------------------
    def _get_port_attribute(self, attr):
        """Read a dynamic report attribute."""
        name = attr.get_name().split("_")
        if name[3] == "serial":
            return self._qsfp_serials[name[2]]
        if name[3] == "temperature":
            return self._qsfp_temperature[name[2]]
        if name[3] == "txpower":
            return self._qsfp_tx_power[name[2]]
        if name[3] == "rxpower":
            return self._qsfp_rx_power[name[2]]
        if name[3] == "txthroughput":
            return self._tx_throughput[f"{name[2]}/0"]
        if name[3] == "rxthroughput":
            return self._rx_throughput[f"{name[2]}/0"]
        if name[3] == "txpps":
            return self._tx_pps[f"{name[2]}/0"]
        if name[3] == "rxpps":
            return self._rx_pps[f"{name[2]}/0"]
        if name[3] == "up":
            return self._up[f"{name[2]}/0"]
        return None

    def get_temperatures(self):
        """Return the various temperature attributes for health."""
        temperatures = {}
        temperatures[
            "hardware_motherboard_temperature"
        ] = self._average_temperature
        temperatures["hardware_tofino_temperature"] = self._tofino_temperature
        for port in range(1, NUMBER_OF_PHYSICAL_PORTS + 1):
            temperatures[
                f"hardware_port_{port}_temperature"
            ] = self._qsfp_temperature[f"{port}"]
        return temperatures

    def get_powers(self):
        """Return the various temperature attributes for health."""
        powers = {}
        for port in range(1, NUMBER_OF_PHYSICAL_PORTS + 1):
            powers[f"hardware_port_{port}_txpower"] = self._qsfp_tx_power[
                f"{port}"
            ]
            powers[f"hardware_port_{port}_rxpower"] = self._qsfp_rx_power[
                f"{port}"
            ]
        return powers

    def get_throughput(self):
        """Return the various throughput for health."""
        throughput = {}
        for port in range(1, NUMBER_OF_PHYSICAL_PORTS + 1):
            throughput[
                f"process_port_{port}_txthroughput"
            ] = self._tx_throughput[f"{port}/0"]
            throughput[
                f"process_port_{port}_rxthroughput"
            ] = self._rx_throughput[f"{port}/0"]
        return throughput

    def get_ports_up(self):
        """Return the various percentage attributes for health."""
        name_list_of_ports_up = [
            "function_alveo_port_up",
            "function_pss_port_up",
            "function_pst_port_up",
            "function_sdp_port_up",
            "function_sps_port_up",
        ]
        list_of_ports_up = [
            self._alveo_port_up,
            self._pss_port_up,
            self._pst_port_up,
            self._sdp_port_up,
            self._sps_port_up,
        ]
        ports_up = {}
        for index, attr in enumerate(name_list_of_ports_up):
            ports_up[attr] = list_of_ports_up[index]
        return ports_up

    def get_code(self):
        """Return the various code boolean for health."""
        return {
            "function_p4_code_version_present": self._p4_code_version_present
        }

    def _set_port_attribute(self, attr):
        """Read a dynamic report attribute."""

    # --------
    # Commands
    # --------
    def init_command_objects(self):
        """
        Initialises the command handlers for commands supported by this device.
        """
        super().init_command_objects()
        # self.register_command_object("GetSimpleTable", self.GetSimpleTable(*device_args))
        # self.register_command_object("GetSpeadTable", self.GetSpeadTable(*device_args))
        # self.register_command_object("GetCounterIngress", self.GetCounterIngress(*device_args))
        # self.register_command_object("AddRoutes", self.AddRoutesCommand(*device_args))
        # self.register_command_object("DelRoutes", self.DelRoutesCommand(*device_args))
        self.register_command_object(
            "AddSpeadUnicastEntry",
            self.AddSpeadUnicastEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "UpdateSpeadUnicastEntry",
            self.UpdateSpeadUnicastEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "RemoveSpeadUnicastEntry",
            self.RemoveSpeadUnicastEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "ClearSpeadUnicastTable",
            self.ClearSpeadUnicastTableCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "AddSpeadMultiplierEntry",
            self.AddSpeadMultiplierEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "RemoveSpeadMultiplierEntry",
            self.RemoveSpeadMultiplierEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "UpdateSpeadMultiplierEntry",
            self.UpdateSpeadMultiplierEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "ClearSpeadMultiplierTable",
            self.ClearSpeadMultiplierTableCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "AddPSREntry",
            self.AddPSREntryCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "UpdatePSREntry",
            self.UpdatePSREntryCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "RemovePSREntry",
            self.RemovePSREntryCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "ClearPSRTable",
            self.ClearPSRTableCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "AddSDPIPEntry",
            self.AddSDPIPEntryCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "UpdateSDPIPEntry",
            self.UpdateSDPIPEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "RemoveSDPIPEntry",
            self.RemoveSDPIPEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "ClearSDPIPTable",
            self.ClearSDPIPTableCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "AddSDPMACEntry",
            self.AddSDPMACEntryCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "UpdateSDPMACEntry",
            self.UpdateSDPMACEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "RemoveSDPMACEntry",
            self.RemoveSDPMACEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "ClearSDPMACTable",
            self.ClearSDPMACTableCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "AddARPEntry",
            self.AddARPEntryCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "UpdateARPEntry",
            self.UpdateARPEntryCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "ClearARPTable",
            self.ClearARPTableCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "RemoveARPEntry",
            self.RemoveARPEntryCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "AddBasicEntry",
            self.AddBasicEntryCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "UpdateBasicEntry",
            self.UpdateBasicEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "ClearBasicTable",
            self.ClearBasicTableCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "RemoveBasicEntry",
            self.RemoveBasicEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "AddIPToResolve",
            self.AddIPToResolveCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "AddPortsToSDPARP",
            self.AddPortsToSDPARPCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "RegisterCallbackAllocator",
            self.RegisterCallbackAllocatorCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "UnsubscribeAllocator",
            self.UnsubscribeAllocatorCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "ConnectToSwitch",
            self.ConnectToSwitchCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "LoadPorts",
            self.LoadPortsCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "RemovePorts",
            self.RemovePortsCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "ResetPortStatistics",
            self.ResetPortStatisticsCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "AddPTPEntry",
            self.AddPTPEntryCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "UpdatePTPEntry",
            self.UpdatePTPEntryCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "ClearPTPTable",
            self.ClearPTPTableCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "AddPortsToPTP",
            self.AddPortsToPTPCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "AddPTPClockPort",
            self.AddPTPClockPortCommand(tango_device=self, logger=self.logger),
        )
        self.register_command_object(
            "AddSubStationEntry",
            self.AddSubStationEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "RemoveSubStationEntry",
            self.RemoveSubStationEntryCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "ClearSubStationTable",
            self.ClearSubStationTableCommand(
                tango_device=self, logger=self.logger
            ),
        )
        self.register_command_object(
            "AddPortMapping",
            self.AddPortMappingCommand(tango_device=self, logger=self.logger),
        )

    def update_arp_replies_attr(self):
        """Get new attribute value and push change event"""

        while True:
            if self.connector is not None:
                arp_replies = self.connector.get_arp_resolver_list()
                if self._arp_replies != arp_replies:
                    self.push_change_event(
                        "arp_replies", json.dumps(arp_replies)
                    )
                    message = f"New Version of arp_replies {arp_replies}"
                    self.logger.info(message)
                    for key, value in arp_replies.items():
                        if value["mac"] != "00:00:00:00:00:00":
                            self.connector.update_sdp_mac_table_entry(
                                key, value["mac"]
                            )
                    self._arp_replies = deepcopy(arp_replies)
            sleep(1)

    def update_from_monitor(
        self, rx_throughput, tx_throughput, rx_pps, tx_pps, up
    ):
        """Extract throughput and PPS from health status"""
        self._rx_throughput = deepcopy(rx_throughput)
        self._tx_throughput = deepcopy(tx_throughput)
        self._rx_pps = deepcopy(rx_pps)
        self._tx_pps = deepcopy(tx_pps)
        self._up = deepcopy(up)
        self.update_percentage_ports()

    def update_percentage_ports(self):
        """Get new attribute health state and status then push event if required"""
        list_of_ports_up = [
            self._alveo_port_up,
            self._pss_port_up,
            self._pst_port_up,
            self._sdp_port_up,
            self._sps_port_up,
        ]
        name_list_of_ports_up = [
            "function_alveo_port_up",
            "function_pss_port_up",
            "function_pst_port_up",
            "function_sdp_port_up",
            "function_sps_port_up",
        ]
        for index, attr in enumerate(
            [
                self._list_alveo_port,
                self._list_pss_port,
                self._list_pst_port,
                self._list_sdp_port,
                self._list_sps_port,
            ]
        ):
            ports_up = 0
            for port in attr:
                ports_up += self._up[port]
            percentage_port = (ports_up * 100) / len(attr)
            if list_of_ports_up[index] != percentage_port:
                list_of_ports_up[index] = percentage_port
                self.set_change_event(
                    name_list_of_ports_up[index], True, False
                )
                self.set_archive_event(
                    name_list_of_ports_up[index], True, False
                )

    def update_health_status_and_state(self, health_status):
        """Get new attribute health state and status then push event if required"""
        self._health_status = deepcopy(health_status)

    def update_information_from_bfshell(
        self,
        qsfp_serials,
        qsfp_temperature,
        qsfp_rx_power,
        qsfp_tx_power,
        tofino_temperature,
        average_temp,
        serial_number,
    ):
        """Get information from the BFshell."""
        self._qsfp_serials = deepcopy(qsfp_serials)
        self._qsfp_temperature = deepcopy(qsfp_temperature)
        self._qsfp_rx_power = deepcopy(qsfp_rx_power)
        self._qsfp_tx_power = deepcopy(qsfp_tx_power)
        self._tofino_temperature = tofino_temperature
        self._average_temperature = average_temp
        self._serial_number = serial_number

    def advanced_sps_telemetry(self, sps_rates):
        """Get new report from the P4 switch about SPS traffic."""
        self._sps_rates = deepcopy(sps_rates)

    def add_port_at_startup(self, ports_to_add: str):
        """
        Enable ports at startup if ports not already configured.

        :param ports_to_add: a string where each word contains a port configuration
          e.g. "1/0-10G-none-disable-84_c7_8f_03_79_b2 2/0-10G-none-disable-84_c7_8f_03_79_b2"
        """
        ports = []
        fib = {}
        for line in ports_to_add.split(" "):
            configuration = line.split("-")
            config = {
                "port": configuration[0],
                "speed": configuration[1],
                "fec": configuration[2],
                "autoneg": configuration[3],
                "mac": configuration[4],
            }
            success, stat = self.connector.get_port_statistics(
                configuration[0]
            )

            if success:
                continue
            # Convert all keys to lowercase
            config = {key.lower(): value for key, value in config.items()}

            if "port" in config:
                re_match = front_panel_regex.match(config["port"])
                if not re_match:
                    self.logger.info(
                        "Invalid port {}".format(config["port"]),
                    )

                fp_port = int(re_match.group(1))
                fp_lane = int(re_match.group(2))
            else:
                self.logger.info(
                    "No port number given",
                )
                continue

            if "speed" in config:
                try:
                    speed = int(
                        config["speed"].upper().replace("G", "").strip()
                    )
                except ValueError:
                    self.logger.info(
                        "Invalid speed for port {}".format(config["port"]),
                    )

                if speed not in [10, 25, 40, 50, 100, 400]:
                    self.logger.error(
                        "Port {} speed must be one of 10G,25G,40G,50G,100G,400G".format(
                            config["port"]
                        ),
                    )
                    speed = 100
            else:
                speed = 100

            if "fec" in config:
                fec = config["fec"].lower().strip()
                if fec not in ["none", "fc", "rs"]:
                    self.logger.error(
                        "Port {} fec must be one of none, fc, rs".format(
                            config["port"]
                        ),
                    )
                    fec = "none"

            if "autoneg" in config:
                an = config["autoneg"].lower().strip()
                if an not in ["default", "enable", "disable"]:
                    self.logger.error(
                        "Port {} autoneg must be one of default, enable, disable".format(
                            config["port"]
                        ),
                    )
                    an = "default"
            else:
                an = "default"

            if "mac" not in config:
                self.logger.info(
                    "Missing MAC address for port {}".format(config["port"]),
                )
                config["mac"] = "aa:bb:cc:dd:ee:ff"

            success, dev_port = self.connector.ports.get_dev_port(
                fp_port, fp_lane
            )
            if success:
                fib[dev_port] = config["mac"].upper()
            else:
                self.logger.info(dev_port)

            ports.append((fp_port, fp_lane, speed, fec, an))
        success, error_msg = self.connector.load_ports(ports)
        if not success:
            self.logger.error(error_msg)
        if not ports:
            self.logger.error("Missing Port configuration")

    class InitCommand(SKABaseDevice.InitCommand):
        def do(self):
            """Initialises the attributes and properties of the LowCbfNetwork."""
            super().do()
            device = self._device
            device._version_id = release.version
            device._spead_unicast_routing_table = ""
            device._spead_multiplier_routing_table = ""
            device._psr_routing_table = ""
            device._arp_routing_table = ""
            device._basic_routing_table = ""
            device._packet_rate = 0.0
            device._packets_lost = ""
            device._packet_loss_rate = 0.0
            device._byte_rate = 0.0
            device._bytes_lost = ""
            device._bytes_loss_rate = 0.0
            device._static_config = ""
            device._dynamic_routes = 0
            device._route_counters = ""
            device._port_status = ""
            device._multicast_session = ""
            device._sub_station_table = ""
            device._sdp_ip_table = ""
            device._sdp_mac_table = ""
            device._arp_replies = {}
            device._health_status = {}
            device._rx_throughput = {}
            device._tx_throughput = {}
            device._rx_pps = {}
            device._tx_pps = {}
            device._up = {}
            device.connector = None
            device.allocator = None
            device._serial_number = ""
            device._average_temperature = 0.0
            device._tofino_temperature = 0.0
            device._list_alveo_port = set()
            device._list_pss_port = set()
            device._list_pst_port = set()
            device._list_sdp_port = set()
            device._list_sps_port = set()
            device._qsfp_tx_power = {}
            device._qsfp_rx_power = {}
            device._qsfp_temperature = {}
            device._qsfp_serials = {}
            device._list_configured_port = []
            device._bfrt_shell_address = device.bfrt_shell_address
            device._bfrt_address = device.bfrt_address
            device._allocator_address = device.allocator_address
            device._subscription_allocator_sps = 0
            device._subscription_allocator_sdp = 0
            device._allocator_retries_connection = 0
            device._name = device.switch
            device._reporting_interval = 10
            device._sps_rates = {}
            device._p4_code_version_present = True
            device._switch_monitor = SwitchMonitor(device, device.connector)
            device._health_state = HealthState.OK
            device._health_hardware = HealthState.OK
            device._health_function = HealthState.OK
            device._health_process = HealthState.OK
            device._alveo_port_up = 100.0
            device._pss_port_up = 100.0
            device._pst_port_up = 100.0
            device._sdp_port_up = 100.0
            device._sps_port_up = 100.0
            device._attr_overrides = {}
            """Attribute Overrides. Key=attribute name, Value=value to force."""

            if (device.hardware_connections is None) or (
                len(device.hardware_connections) == 0
            ):
                device.logger.info("Using built-in default connection list")
                cnx_list = default_connection_list()
            else:
                device.logger.info("Using connection list from helm chart")
                cnx_list = device.hardware_connections
            (
                device._list_alveo_port,
                device._list_pss_port,
                device._list_pst_port,
                device._list_sdp_port,
                device._list_sps_port,
            ) = initialise_list_ports(cnx_list)
            if device.bfrt_address != "rand_address:rand_port":
                if device.connector is None:
                    device.connector = SpeadConnector(
                        "low_cbf",
                        device.bfrt_address.split(":")[0],
                        device.bfrt_address.split(":")[1],
                    )
                    message = "Connecting to switch"
                    device.logger.info(message)
                else:
                    message = "Already connected to the switch"
                    device.logger.info(message)
                if device.ports_configuration != "do_not_configure_ports":
                    device.add_port_at_startup(device.ports_configuration)
                device._switch_monitor = SwitchMonitor(
                    device, device.connector
                )
                if device.ebpf_configuration != "no_advanced_telemetry":
                    apt_update = ["apt", "update"]
                    check_call(
                        apt_update,
                        stdout=open(os.devnull, "wb"),
                        stderr=STDOUT,
                    )
                    check = check_output(["uname", "-r"])
                    kernel_version = str(check).split("'")[1].split("\\n")[0]
                    apt_install_kernel = [
                        "apt",
                        "install",
                        "-y",
                        f"linux-headers-{kernel_version}",
                    ]
                    check_call(
                        apt_install_kernel,
                        stdout=open(os.devnull, "wb"),
                        stderr=STDOUT,
                    )
                    device._reporting_interval = int(
                        device.ebpf_configuration.split(":")[1]
                    )
                    device.connector.start_ebpf_telemetry_table(
                        interface=device.ebpf_configuration.split(":")[0],
                        interval=device._reporting_interval,
                    )

            if device.bfrt_shell_address != "rand_address:rand_port":
                dtype_for_attribute = {
                    "serial": str,
                    "temperature": int,
                    "txpower": str,
                    "rxpower": str,
                    "up": int,
                    "rxpps": int,
                    "txpps": int,
                    "rxthroughput": int,
                    "txthroughput": int,
                }
                for port in range(1, NUMBER_OF_PHYSICAL_PORTS + 1):
                    device._qsfp_tx_power[
                        f"{port}"
                    ] = "-40.0, -40.0, -40.0, -40.0"
                    device._qsfp_rx_power[
                        f"{port}"
                    ] = "-40.0, -40.0, -40.0, -40.0"
                    device._qsfp_temperature[f"{port}"] = 0
                    device._qsfp_serials[f"{port}"] = "NotRetrieved"
                    device._rx_throughput[f"{port}/0"] = 0
                    device._tx_throughput[f"{port}/0"] = 0
                    device._rx_pps[f"{port}/0"] = 0
                    device._tx_pps[f"{port}/0"] = 0
                    device._up[f"{port}/0"] = 0
                    # health hardware
                    for name_suffix in [
                        "temperature",
                        "txpower",
                        "rxpower",
                    ]:
                        name = f"hardware_port_{port}_{name_suffix}"
                        att = attribute(
                            name=name,
                            dtype=dtype_for_attribute[name_suffix],
                            fget=device._get_port_attribute,
                            fset=device._set_port_attribute,
                            access=AttrWriteType.READ,
                        )
                        device.add_attribute(att)
                        device.set_change_event(name, True, False)
                        device.set_archive_event(name, True, False)
                    # health function (no port attributes)
                    # health process
                    for name_suffix in [
                        "rxthroughput",
                        "txthroughput",
                    ]:
                        name = f"process_port_{port}_{name_suffix}"
                        att = attribute(
                            name=name,
                            dtype=dtype_for_attribute[name_suffix],
                            fget=device._get_port_attribute,
                            fset=device._set_port_attribute,
                            access=AttrWriteType.READ,
                        )
                        device.add_attribute(att)
                        device.set_change_event(name, True, False)
                        device.set_archive_event(name, True, False)
                    # non-health attributes
                    for name_suffix in [
                        "serial",
                        "up",
                        "rxpps",
                        "txpps",
                    ]:
                        name = f"diagnostics_port_{port}_{name_suffix}"
                        att = attribute(
                            name=name,
                            dtype=dtype_for_attribute[name_suffix],
                            fget=device._get_port_attribute,
                            fset=device._set_port_attribute,
                            access=AttrWriteType.READ,
                        )
                        device.add_attribute(att)
                        device.set_change_event(name, True, False)
                        device.set_archive_event(name, True, False)

                device._switch_monitor.start_shell_monitoring(
                    device.bfrt_shell_address
                )

                # device._shell_registration = Thread(
                #     target=device.update_information_from_bfshell,
                #     args=(device.bfrt_shell_address,),
                # )
                # device._shell_registration.start()
            if device.allocator_address != "rand_url":
                device._allocator_registration = Thread(
                    target=device.allocator_connect_and_keep_alive,
                    args=(device.allocator_address,),
                )
                device._allocator_registration.start()

            device.set_change_event("arp_replies", True, False)
            for attri in [
                "health_status",
                "health_hardware",
                "health_function",
                "health_process",
                "port_rx_throughput",
                "port_tx_throughput",
                "port_rx_pps",
                "port_tx_pps",
                "spsRates",
                "unit_serial",
                "hardware_motherboard_temperature",
                "hardware_tofino_temperature",
                "function_alveo_port_up",
                "function_pss_port_up",
                "function_pst_port_up",
                "function_sdp_port_up",
                "function_sps_port_up",
            ]:
                device.set_change_event(attri, True, False)
                device.set_archive_event(attri, True, False)

            device._arp_replies_thread = Thread(
                target=device.update_arp_replies_attr
            )
            device._arp_replies_thread.start()

            device._switch_monitor.start_health_monitoring()
            device._switch_monitor.start_advanced_telemetry_monitoring()

            device.hw_health_mon = HardwareHealth(
                HW_HEALTH_CONFIG_FILE,
                device,
                device.logger,
            )
            device.hw_health_mon.start_monitoring(HW_HEALTH_CHECK_PERIOD)

            device.fn_health_mon = FunctionHealth(
                FUNC_HEALTH_CONFIG_FILE,
                device,
                device.logger,
            )
            device.fn_health_mon.start_monitoring(FUNC_HEALTH_CHECK_PERIOD)

            device.proc_health_mon = ProcessHealth(
                PROC_HEALTH_CONFIG_FILE,
                device,
                device.logger,
            )
            device.proc_health_mon.start_monitoring(PROC_HEALTH_CHECK_PERIOD)

            message = "init complete, address {}".format(device.bfrt_address)
            device.logger.info(message)
            return (ResultCode.OK, message)

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddSpeadUnicastEntry(self, argin):
        """
        Add SPEAD unicast entry.

        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": {"port": "12/0"}}]}'
            We assume src is an engineering subarray and dst is a port.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("AddSpeadUnicastEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class AddSpeadUnicastEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["spead"]
            device.logger.info(
                "AddSpeadUnicastEntry request {}".format(routes)
            )
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                device.connector.add_spead_table_entry(
                    int(route["src"]["frequency"]),
                    int(route["src"]["beam"]),
                    int(route["src"]["sub_array"]),
                    front_panel_regex.match(route["dst"]["port"]),
                )
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to remove.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def RemoveSpeadUnicastEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to remove.
            e.g. '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }}]}'
            src is required, dst is optional
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("RemoveSpeadUnicastEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class RemoveSpeadUnicastEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["spead"]
            device.logger.info(
                "RemoveSpeadUnicastEntry request {}".format(routes)
            )
            for route in routes:
                device.logger.info("Removing route {}".format(route))
                try:
                    device.connector.remove_spead_table_entry(
                        int(route["src"]["frequency"]),
                        int(route["src"]["beam"]),
                        int(route["src"]["sub_array"]),
                    )
                except Exception:
                    return (
                        ResultCode.FAILED,
                        "failing to remove Spead unicasting",
                    )
            message = "Routes_Removed"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def UpdateSpeadUnicastEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": {"port": "12/0"}}]}'
            We assume src is an engineering subarray and dst is a port.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("UpdateSpeadUnicastEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class UpdateSpeadUnicastEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["spead"]
            device.logger.info(
                "AddSpeadUnicastEntry request {}".format(routes)
            )
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                device.connector.update_spead_table_entry(
                    int(route["src"]["frequency"]),
                    int(route["src"]["beam"]),
                    int(route["src"]["sub_array"]),
                    front_panel_regex.match(route["dst"]["port"]),
                )
            message = "Routes_Added"
            device.logger.info(message)
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more routing rules to delete."
        "If a route is partially specified, all matching routes will be deleted.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def ClearSpeadUnicastTable(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to delete.
            If a route is partially specified, all matching routes will be deleted.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("ClearSpeadUnicastTable")
        result_code, message = handler(argin)
        return [result_code], [message]

    class ClearSpeadUnicastTableCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            device.logger.info("Deleting all rules in SPEAD Unicast")

            device.connector.clear_spead_table()
            message = "Routes_Deleted"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddMultiplierUnicastEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": {"port_bf": "12/0",
            "port_corr": "13/0"}}]}' (DEPRECATED)
             '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": ["12/0", "13/0"]}]}'
            We assume src is an engineering subarray and dst is a port.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("AddSpeadMultiplierEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class AddSpeadMultiplierEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["spead"]
            device.logger.info(
                "AddSpeadMultiplierEntry request {}".format(routes)
            )
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                destination_ports = []
                if isinstance(route["dst"], dict):
                    destination_ports.append(
                        front_panel_regex.match(route["dst"]["port_bf"])
                    )
                    destination_ports.append(
                        front_panel_regex.match(route["dst"]["port_corr"])
                    )
                elif isinstance(route["dst"], list):
                    for port in route["dst"]:
                        destination_ports.append(front_panel_regex.match(port))
                device.connector.add_spead_multiplier_table_entry(
                    int(route["src"]["frequency"]),
                    int(route["src"]["beam"]),
                    int(route["src"]["sub_array"]),
                    destination_ports,
                )
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to remove.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def RemoveSpeadMultiplierEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to remove.
                e.g. '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }}]}'
                src is required, dst is optional
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("RemoveSpeadMultiplierEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class RemoveSpeadMultiplierEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["spead"]
            device.logger.info(
                "RemoveMultiplierUnicastEntry request {}".format(routes)
            )
            for route in routes:
                device.logger.info("Removing route {}".format(route))
                try:
                    device.connector.remove_spead_multiplier_table_entry(
                        int(route["src"]["frequency"]),
                        int(route["src"]["beam"]),
                        int(route["src"]["sub_array"]),
                    )
                except Exception:
                    return ResultCode.FAILED, "Failing to remove SPEAD entry"
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def UpdateMultiplierUnicastEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": {"port_bf": "12/0",
            "port_corr": "13/0"}}]}' (DEPRECATED)
             '{"spead": [{"src": {"frequency": 123, "beam": 12, "sub_array": 1 }, "dst": ["12/0", "13/0"]}]}'
            We assume src is an engineering subarray and dst is a port.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("UpdateSpeadMultiplierEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class UpdateSpeadMultiplierEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["spead"]
            device.logger.info(
                "UpdateSpeadMultiplierEntry request {}".format(routes)
            )
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                destination_ports = []
                if isinstance(route["dst"], dict):
                    destination_ports.append(
                        front_panel_regex.match(route["dst"]["port_bf"])
                    )
                    destination_ports.append(
                        front_panel_regex.match(route["dst"]["port_corr"])
                    )
                elif isinstance(route["dst"], list):
                    for port in route["dst"]:
                        destination_ports.append(front_panel_regex.match(port))
                device.connector.update_spead_multiplier_table_entry(
                    int(route["src"]["frequency"]),
                    int(route["src"]["beam"]),
                    int(route["src"]["sub_array"]),
                    destination_ports,
                )
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more routing rules to delete."
        "If a route is partially specified, all matching routes will be deleted.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def ClearSpeadMultiplierTable(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to delete.
            If a route is partially specified, all matching routes will be deleted.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("ClearSpeadMultiplierTable")
        result_code, message = handler(argin)
        return [result_code], [message]

    class ClearSpeadMultiplierTableCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            device.logger.info("Deleting all rules in SPEAD Multiplier")

            device.connector.clear_spead_multiplier_table()
            message = "Routes_Deleted"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddPSREntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"psr": [{"src": {"beam": 12 }, "dst": {"port": "12/0"}}]}' (Deprecated)
            '{"psr": [{"src": {"beam": 12 }, "dst": {"port": "12/0", "udp_port": 9510}}]}'
            We assume src is an engineering subarray and dst is a port.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("AddPSREntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class AddPSREntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["psr"]
            device.logger.info("AddPSREntry request {}".format(routes))
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                udp_port = (
                    9510
                    if "udp_port" not in route["dst"].keys()
                    else route["dst"]["udp_port"]
                )
                device.connector.add_psr_table_entry(
                    int(route["src"]["beam"]),
                    front_panel_regex.match(route["dst"]["port"]),
                    udp_port,
                )
            message = "PSR_Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more psr rules to remove.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def RemovePSREntry(self, argin):
        """
        :param argin: JSON String describing one or more psr rules to remove.
            e.g. '{"psr": [{"src": {"beam": 12 }}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("RemovePSREntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class RemovePSREntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["psr"]
            device.logger.info("RemovePSREntry request {}".format(routes))
            for route in routes:
                device.logger.info("Removing route to beam {}".format(route))
                try:
                    device.connector.remove_psr_table_entry(
                        int(route["src"]["beam"]),
                    )
                except Exception:
                    return ResultCode.FAILED, "failed to remove PSR Entry"
            message = "PSR_Routes_Removed"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def UpdatePSREntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"psr": [{"src": {"beam": 12 }, "dst": {"port": "12/0"}}]}' (Deprecated)
            '{"psr": [{"src": {"beam": 12 }, "dst": {"port": "12/0", "udp_port": 9510}}]}'
            We assume src is an engineering subarray and dst is a port.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("UpdatePSREntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class UpdatePSREntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["psr"]
            device.logger.info("UpdatePSREntry request {}".format(routes))
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                udp_port = (
                    9510
                    if "udp_port" not in route["dst"].keys()
                    else route["dst"]["udp_port"]
                )
                device.connector.update_psr_table_entry(
                    int(route["src"]["beam"]),
                    front_panel_regex.match(route["dst"]["port"]),
                    udp_port,
                )
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more routing rules to delete."
        "If a route is partially specified, all matching routes will be deleted.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def ClearPSRTable(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to delete.
            If a route is partially specified, all matching routes will be deleted.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("ClearPSRTable")
        result_code, message = handler(argin)
        return [result_code], [message]

    class ClearPSRTableCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            device.logger.info("Deleting all rules in SPEAD Unicast")

            device.connector.clear_psr_table()
            message = "Routes_Deleted"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddARPEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
                      e.g. '{"arp": [{"ip": {"address": "192.168.1.1"},
                             "mac": {"address": "aa:bb:cc:dd:ee:ff"}}]}'
                      We assume src is an engineering subarray and dst is a port.
        :type argin: string
        :return: None
        """
        handler = self.get_command_object("AddARPEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class AddARPEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["arp"]
            device.logger.info("AddARPEntry request {}".format(routes))
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                device.connector.add_arp_table_entry(
                    route["ip"]["address"],
                    route["mac"]["address"],
                )
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more ARP rules to remove.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def RemoveARPEntry(self, argin):
        """
        :param argin: String describing one or more ARP rules to remove..
            e.g. '{"arp": [{"ip": {"address": "192.168.1.1"}, "mac": {"address": "aa:bb:cc:dd:ee:ff"}}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("RemoveARPEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class RemoveARPEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["arp"]
            device.logger.info("RemoveARPEntry request {}".format(routes))
            for route in routes:
                device.logger.info("Removing request {}".format(route))
                try:
                    device.connector.del_arp_table_entry(
                        route["ip"]["address"],
                    )
                except CommandError:
                    return ResultCode.FAILED, "Error removing ARP requests"
            message = "Routes_Removed"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def UpdateARPEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"arp": [{"ip": {"address": "192.168.1.1"}, "mac": {"address": "aa:bb:cc:dd:ee:ff"}}]}'
            We assume src is an engineering subarray and dst is a port.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("UpdateARPEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class UpdateARPEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["arp"]
            device.logger.info("ARPEntry request {}".format(routes))
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                device.connector.update_arp_table_entry(
                    route["ip"]["address"],
                    route["mac"]["address"],
                )
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more routing rules to delete."
        "If a route is partially specified, all matching routes will be deleted.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def ClearARPTable(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to delete.
            If a route is partially specified, all matching routes will be deleted.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("ClearARPTable")
        result_code, message = handler(argin)
        return [result_code], [message]

    class ClearARPTableCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            device.logger.info("Deleting all rules in ARP")

            device.connector.clear_arp_table()
            message = "Routes_Deleted"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddBasicEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
                      e.g. '{"basic": [{"src": {"port": "1/0"}, "dst": {"port": "2/0"}}]}'
                      We assume src is an engineering subarray and dst is a port.
        :type argin: string
        :return: None
        """
        handler = self.get_command_object("AddBasicEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class AddBasicEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["basic"]
            device.logger.info("BasicEntry request {}".format(routes))
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                device.connector.add_simple_table_entry(
                    front_panel_regex.match(route["src"]["port"]),
                    front_panel_regex.match(route["dst"]["port"]),
                )
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more basic rules to remove.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def RemoveBasicEntry(self, argin):
        """
        :param argin: String describing one or more basic rules to remove.
            e.g. '{"basic": [{"src": {"port": "1/0"}}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("RemoveBasicEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class RemoveBasicEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["basic"]
            device.logger.info(
                "BasicEntry for ingress port  {}".format(routes)
            )
            for route in routes:
                device.logger.info(
                    "Removing route for ingress port {}".format(route)
                )
                try:
                    device.connector.del_simple_table_entry(
                        front_panel_regex.match(route["src"]["port"]),
                    )
                except Exception:
                    return ResultCode.FAILED, "removing basic route failed"
            message = "Routes_Removed"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def UpdateBasicEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"basic": [{"src": {"port": "1/0"}, "dst": {"port": "2/0"}}]}'
            We assume src is an engineering subarray and dst is a port.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("UpdateBasicEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class UpdateBasicEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["basic"]
            device.logger.info("AddBasicEntry request {}".format(routes))
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                device.connector.update_simple_table_entry(
                    front_panel_regex.match(route["src"]["port"]),
                    front_panel_regex.match(route["dst"]["port"]),
                )
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more routing rules to delete."
        "If a route is partially specified, all matching routes will be deleted.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def ClearBasicTable(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to delete.
            If a route is partially specified, all matching routes will be deleted.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("ClearBasicTable")
        result_code, message = handler(argin)
        return [result_code], [message]

    class ClearBasicTableCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            device.logger.info("Deleting all rules in Basic Table")

            device.connector.clear_simple_table()
            message = "Routes_Deleted"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more IP Addresses to ARP resolve..",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddIPToResolve(self, argin):
        """
        :param argin: JSON String describing one or more IP Addresses to ARP resolve.
            e.g. '{"IP": [{"address": "192.168.1.1"}]}'
        :type argin: str
        """
        handler = self.get_command_object("AddIPToResolve")
        result_code, message = handler(argin)
        return [result_code], [message]

    class AddIPToResolveCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["IP"]
            device.logger.info("AddIPToResolve request {}".format(routes))
            for route in routes:
                device.logger.info("Adding IP To Resolve {}".format(route))
                device.connector.add_ip_address_to_resolver(route["address"])
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more Port connected to the SDP network",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddPortsToSDPARP(self, argin):
        """
        :param argin: JSON String describing one or more ports connected to SDP.
            e.g. '{"SDP": [{"port": "10/0"}]}'
        :type argin: str
        """
        handler = self.get_command_object("AddPortsToSDPARP")
        result_code, message = handler(argin)
        return [result_code], [message]

    class AddPortsToSDPARPCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["SDP"]
            device.logger.info("AddPortsToSDPARP request {}".format(routes))
            ports_to_add = []
            for route in routes:
                device.logger.info(
                    "Adding Port To SDP network {}".format(route)
                )
                ports_to_add.append(front_panel_regex.match(route["port"]))
            device.connector.configure_multicast_for_SDP(ports_to_add)
            message = "Ports_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing the allocator address",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def RegisterCallbackAllocator(self, argin):
        """
        :param argin: JSON String describing one or more ports connected to SDP.
            e.g. '{"Allocator": "tango-databaseds.ska-low-cbf:10000/low-cbf/allocator/0"}'
        :type argin: str
        """
        handler = self.get_command_object("RegisterCallbackAllocator")
        result_code, message = handler(argin)
        return [result_code], [message]

    class RegisterCallbackAllocatorCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            allocator = json.loads(argin)["Allocator"]
            device.logger.info(
                "RegisterCallbackAllocator request {}".format(allocator)
            )
            device._allocator_registration = Thread(
                target=device.allocator_connect_and_keep_alive,
                args=(allocator,),
            )
            device._allocator_registration.start()
            message = "Allocator callback"
            return ResultCode.OK, message

    @command(
        dtype_in=None,
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def UnsubscribeAllocator(self):
        """
        Unsubscribe from the allocator.
        """
        handler = self.get_command_object("UnsubscribeAllocator")
        result_code, message = handler()
        return [result_code], [message]

    class UnsubscribeAllocatorCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self):
            device = self._tango_device
            device.logger.info("UnsubscribeAllocator request")
            device.unsubscribe_to_allocator()
            message = "Allocator unsubscription"
            return ResultCode.OK, message

    def unsubscribe_to_allocator(self):
        """
        Unsubscribe from the allocator and reset everything.

        """
        if self.allocator is None:
            self.logger.error("Allocator not available yet")
            return
        self.allocator.unsubscribe_event(self._subscription_allocator_sps)
        self._subscription_allocator_sps = None
        self.allocator.unsubscribe_event(self._subscription_allocator_sdp)
        self._subscription_allocator_sdp = None
        del self.allocator
        self.allocator = None

    def allocator_connect_and_keep_alive(self, allocator_fqdn: str):
        """
        Connect to the connector inside a thread and ping regularly.

        :param allocator_fqdn: Allocator FQDN
        """
        connected_to_allocator = False
        while not connected_to_allocator:
            try:
                self.allocator = tango.DeviceProxy(
                    allocator_fqdn, green_mode=tango.GreenMode.Synchronous
                )
                connected_to_allocator = True
            except Exception as ex:
                self.logger.info(
                    "Unexpected error on DeviceProxy creation %s", str(ex)
                )
                self.logger.info("Retrying in 5 seconds")
                sleep(5)

        self._subscription_allocator_sps = self.allocator.subscribe_event(
            "p4_stn_routes",
            tango.EventType.CHANGE_EVENT,
            self.update_sps_routes,
            stateless=True,
        )
        self._subscription_allocator_sdp = self.allocator.subscribe_event(
            "sdp_routes",
            tango.EventType.CHANGE_EVENT,
            self.update_sdp_routes,
            stateless=True,
        )

        message = "Allocator callback"
        self.logger.info(message)

        while True:
            try:
                if self.allocator is None:
                    self.logger.error("Allocator no longer available")
                    return
                self.allocator.ping()
                self._allocator_retries_connection = 0
                sleep(10)
            except Exception as ex:
                self.logger.info(
                    "Unexpected error on Allocator DeviceProxy ping %s",
                    str(ex),
                )
                self.logger.info("Retrying in 10 seconds")
                self._allocator_retries_connection += 1
                if self._allocator_retries_connection > 10:
                    self.allocator.unsubscribe_event(
                        self._subscription_allocator_sps
                    )
                    self._subscription_allocator_sps = None
                    self.allocator.unsubscribe_event(
                        self._subscription_allocator_sdp
                    )
                    self._subscription_allocator_sdp = None
                    del self.allocator
                    self.allocator = None
                    break
                sleep(10)

    def update_sps_routes(self, route_updates: tango.EventData):
        """
        :param route_updates: routing change event from Allocator
        If routes have been updated the connector is going to update the route in the switch
        :return: None
        """
        if route_updates.err:
            error = route_updates.errors[0]
            print(error)
            return

        # device = self._tango_device
        self.logger.info("received updated route {}".format(route_updates))
        routes = json.loads(self.allocator.p4_stn_routes)["p4_01"]
        if len(routes) == 0:
            self.connector.clear_spead_multiplier_table()
            self.connector.clear_spead_table()
        (
            success,
            installed_routes,
        ) = self.connector.get_spead_multiplier_table_entries()
        if len(installed_routes) != 0:
            self.remove_unused_routes(installed_routes, routes)
        for route in routes:
            self.logger.info("Trying to add P4 Route {}".format(route))
            if self.connector is not None:
                if len(route[1]) == 2:
                    self.connector.update_spead_multiplier_table_entry(
                        int(route[0][2]),  # frequency
                        int(route[0][1]),  # beam
                        int(route[0][0]),  # subbarray
                        front_panel_regex.match(route[1][0]),
                        front_panel_regex.match(route[1][1]),
                    )
                    self.logger.info("P4 Route added {}".format(route))
                elif len(route[1]) == 1:
                    self.connector.update_spead_table_entry(
                        int(route[0][2]),  # frequency
                        int(route[0][1]),  # beam
                        int(route[0][0]),  # subbarray
                        front_panel_regex.match(route[1][0]),
                    )
                    self.logger.info("P4 Route added {}".format(route))
            else:
                self.logger.info("Connection to the switch not yet available")

    def remove_unused_routes(self, installed_routes, deployment_routes):
        # device = self._tango_device
        dictionary_installed_routes = {}
        dictionary_deployment_routes = {}
        self.logger.info("removing potential mismatch routes")
        for installed_route in installed_routes:
            dictionary_installed_routes[
                "{} {} {}".format(
                    installed_route["Frequency"],
                    installed_route["Beam"],
                    installed_route["Sub_array"],
                )
            ] = False
        for deployment_route in deployment_routes:
            dictionary_deployment_routes[
                "{} {} {}".format(
                    deployment_route[0][2],
                    deployment_route[0][1],
                    deployment_route[0][0],
                )
            ] = False
        for key, value in dictionary_installed_routes.items():
            if key not in dictionary_deployment_routes:
                self.logger.info("removing routes {}".format(key))
                match = key.split(" ")
                self.connector.remove_spead_multiplier_table_entry(
                    int(match[0]), int(match[1]), int(match[2])
                )

    def update_sdp_routes(self, route_updates):
        """
        Process an SDP route update event received from the Allocator.

        If routes have been updated the connector is going to update the route in the switch.

        :param route_updates: 'String'
        :return: None
        """
        if route_updates.err:
            error = route_updates.errors[0]
            print(error)
            return

        # device = self._tango_device
        self.logger.info("received updated route {}".format(route_updates))
        allocator_sdp_routes = json.loads(route_updates.attr_value.value)
        self.logger.info(f"New SDP port {json.dumps(allocator_sdp_routes)}")
        sdp_configuration = allocator_sdp_routes["p4_01"]

        arp_to_request = allocator_sdp_routes["arp_rq"]
        sdp_ports = sdp_configuration["sdp_ports"]
        sdp_routes = sdp_configuration["routes"]
        if len(sdp_routes) == 0:
            self.connector.clear_sdp_ip_table()
        (
            success,
            installed_routes,
        ) = self.connector.get_sdp_ip_table_entries()
        if success:
            if len(installed_routes) != 0:
                self.remove_unused_sdp_routes(installed_routes, sdp_routes)
            for route in sdp_routes:
                self.logger.info("Trying to add P4 Route {}".format(route))
                self.connector.update_sdp_ip_table_entry(
                    route[0], front_panel_regex.match(route[1])
                )

        else:
            self.logger.info(
                "Error when retrieving installed SDP routes from the switch"
            )

        sdp_ports_switch = self.connector.get_multicast_configurations()["2"]
        for sdp_port in sdp_ports:
            if sdp_port not in sdp_ports_switch:
                self.logger.info(f"adding port {sdp_port} to ARP resolver ")
                # adding the port to the dedicated ARP request multicast session (id = 2, port offset = 513)
                self.connector.add_port_to_multicast_session(
                    2, 513, front_panel_regex.match(sdp_port)
                )
                sdp_ports_switch.append(sdp_port)
        # need to check that the 2 list are the same now
        if set(sdp_ports_switch) != set(sdp_ports):
            ports_to_remove = list(set(sdp_ports_switch) - set(sdp_ports))
            # TODO implement method to remove port in multicast session
            _ = ports_to_remove  # silence linting warning

        current_arp_resolver_list = self.connector.get_arp_resolver_list()
        for ip_address_to_resolve in arp_to_request:
            if ip_address_to_resolve not in current_arp_resolver_list:
                self.logger.info(
                    "Add IP address {} to ARP resolver".format(
                        ip_address_to_resolve
                    )
                )
                self.connector.add_ip_address_to_resolver(
                    ip_address_to_resolve
                )
                current_arp_resolver_list[ip_address_to_resolve] = {
                    "mac": "00:00:00:00:00:00",
                    "port": 0,
                }
        self.logger.info(
            f"Addresses to resolve need changes {arp_to_request} vs {list(current_arp_resolver_list.keys())}"
        )
        if set(list(current_arp_resolver_list.keys())) != set(arp_to_request):
            address_to_remove = set(
                list(current_arp_resolver_list.keys())
            ) - set(arp_to_request)
            for address in address_to_remove:
                self.connector.remove_ip_address_from_resolver(address)

    def remove_unused_sdp_routes(self, installed_routes, deployment_routes):
        # device = self._tango_device
        dictionary_installed_routes = {}
        dictionary_deployment_routes = {}
        self.logger.info("removing potential mismatch routes")
        for installed_route in installed_routes:
            dictionary_installed_routes[installed_route["IP_Address"]] = False
        for deployment_route in deployment_routes:
            dictionary_deployment_routes[deployment_route[0]] = False
        for key, value in dictionary_installed_routes.items():
            if key not in dictionary_deployment_routes:
                self.logger.info("removing routes {}".format(key))
                self.connector.remove_sdp_ip_table_entry(key)

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing the switch_d agent address",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def ConnectToSwitch(self, argin):
        """
        :param argin: JSON String describing the address to connect to.
            e.g. '{"Switch": "202.9.15.135:50052"}'
        :type argin: str
        """
        handler = self.get_command_object("ConnectToSwitch")
        result_code, message = handler(argin)
        return [result_code], [message]

    class ConnectToSwitchCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            address = json.loads(argin)["Switch"]
            device.logger.info("ConnectToSwitch request {}".format(address))
            message = ""
            if device.connector is None:
                device.connector = SpeadConnector(
                    "low_cbf", address.split(":")[0], address.split(":")[1]
                )
                message = "Connecting to switch"
                device.logger.info(message)
            else:
                message = "Already connected to the switch"
                device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing how to configure physical ports",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def LoadPorts(self, argin):
        """
        :param argin: JSON String describing the physical ports configuration.
            e.g. '{"Physical": [{"port": "10/0", "speed": "100G", "fec": "none", "autoneg": "disable", "mac": "84:c7:8f:03:79:b2" }]}'
        :type argin: str
        """
        handler = self.get_command_object("LoadPorts")
        result_code, message = handler(argin)
        return [result_code], [message]

    class LoadPortsCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            ports = []
            fib = {}
            port_configs = json.loads(argin)["Physical"]
            for config in port_configs:
                # Convert all keys to lowercase
                config = {key.lower(): value for key, value in config.items()}

                if "port" in config:
                    re_match = front_panel_regex.match(config["port"])
                    if not re_match:
                        return (
                            ResultCode.FAILED,
                            "Invalid port {}".format(config["port"]),
                        )

                    fp_port = int(re_match.group(1))
                    fp_lane = int(re_match.group(2))
                else:
                    return (
                        ResultCode.FAILED,
                        "No port number given",
                    )

                if "speed" in config:
                    try:
                        speed = int(
                            config["speed"].upper().replace("G", "").strip()
                        )
                    except ValueError:
                        return (
                            ResultCode.FAILED,
                            "Invalid speed for port {}".format(config["port"]),
                        )

                    if speed not in [10, 25, 40, 50, 100, 400]:
                        return (
                            ResultCode.FAILED,
                            "Port {} speed must be one of 10G,25G,40G,50G,100G,400G".format(
                                config["port"]
                            ),
                        )
                else:
                    speed = 100

                if "fec" in config:
                    fec = config["fec"].lower().strip()
                    if fec not in ["none", "fc", "rs"]:
                        return (
                            ResultCode.FAILED,
                            "Port {} fec must be one of none, fc, rs".format(
                                config["port"]
                            ),
                        )
                else:
                    fec = "none"

                if "autoneg" in config:
                    an = config["autoneg"].lower().strip()
                    if an not in ["default", "enable", "disable"]:
                        return (
                            ResultCode.FAILED,
                            "Port {} autoneg must be one of default, enable, disable".format(
                                config["port"]
                            ),
                        )
                else:
                    an = "default"

                if "mac" not in config:
                    return (
                        ResultCode.FAILED,
                        "Missing MAC address for port {}".format(
                            config["port"]
                        ),
                    )

                success, dev_port = device.connector.ports.get_dev_port(
                    fp_port, fp_lane
                )
                if success:
                    fib[dev_port] = config["mac"].upper()
                else:
                    return (ResultCode.FAILED, dev_port)

                ports.append((fp_port, fp_lane, speed, fec, an))
            success, error_msg = device.connector.load_ports(ports)
            if not success:
                device.logger.error(error_msg)
                return ResultCode.FAILED, "Error during port configuration"
            if not ports:
                return ResultCode.FAILED, "Missing Port configuration"
            message = "Port Configured"
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing the physical ports to remove.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def RemovePorts(self, argin):
        """
        :param argin: JSON String describing the physical ports to remove.
            e.g. '{"Physical": [{"port": "10/0"}]}'
        :type argin: str
        """
        handler = self.get_command_object("RemovePorts")
        result_code, message = handler(argin)
        return [result_code], [message]

    class RemovePortsCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            port_configs = json.loads(argin)["Physical"]
            for config in port_configs:
                if "port" in config:
                    re_match = front_panel_regex.match(config["port"])
                    if not re_match:
                        return (
                            ResultCode.FAILED,
                            "Invalid port {}".format(config["port"]),
                        )

                    device.connector.remove_ports(str(config["port"]))
                else:
                    return (
                        ResultCode.FAILED,
                        "No port number given",
                    )
            message = "Ports Removed"
            return ResultCode.OK, message

    @command(
        dtype_out="DevVarLongStringArray",
    )
    def ResetPortStatistics(self):
        """Reset all port statistics counters"""
        handler = self.get_command_object("ResetPortStatistics")
        result_code, message = handler()
        return [result_code], [message]

    class ResetPortStatisticsCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self):
            device = self._tango_device
            device.connector.reset_ports_statistics()
            message = "Ports Statistics Reset"
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddPTPEntry(self, argin):
        """
        :param argin: JSON String describing one or more PTP routing rules to add.
            e.g. '{"ptp": [{"src": {"port": "1/0"}, "dst": {"port": "2/0"}}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("AddPTPEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class AddPTPEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["ptp"]
            device.logger.info("PTPEntry request {}".format(routes))
            for route in routes:
                device.logger.info("Adding PTP route {}".format(route))
                try:
                    device.connector.add_ptp_table_entry(
                        front_panel_regex.match(route["src"]["port"]),
                        front_panel_regex.match(route["dst"]["port"]),
                    )
                except CommandError as e:
                    return (
                        ResultCode.FAILED,
                        str(e),
                    )
            if not routes:
                return (
                    ResultCode.FAILED,
                    "No PTP port number given",
                )
            message = "PTP_Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def UpdatePTPEntry(self, argin):
        """
        :param argin: JSON String describing one or more PTP routing rules to add.
            e.g. '{"ptp": [{"src": {"port": "1/0"}, "dst": {"port": "2/0"}}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("UpdatePTPEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class UpdatePTPEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["ptp"]
            device.logger.info("AddPTPEntry request {}".format(routes))
            for route in routes:
                device.logger.info("Adding PTP route {}".format(route))
                try:
                    device.connector.update_ptp_table_entry(
                        front_panel_regex.match(route["src"]["port"]),
                        front_panel_regex.match(route["dst"]["port"]),
                    )
                except CommandError as e:
                    return (
                        ResultCode.FAILED,
                        str(e),
                    )
            if not routes:
                return (
                    ResultCode.FAILED,
                    "No PTP port number given",
                )
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more routing rules to delete."
        "If a route is partially specified, all matching routes will be deleted.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def ClearPTPTable(self, argin):
        """
        :param argin: JSON String describing one or more PTP routing rules to delete.
            If a route is partially specified, all matching routes will be deleted.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("ClearPTPTable")
        result_code, message = handler(argin)
        return [result_code], [message]

    class ClearPTPTableCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            device.logger.info("Deleting all rules in Basic Table")

            device.connector.clear_ptp_table()
            message = "Routes_Deleted"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more Alveo ports connected to the PTP network ",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddPortsToPTP(self, argin):
        """
        :param argin: JSON String describing one or more ports connected to PTP.
            e.g. '{"PTPMulti": [{"port": "10/0"}, {"port": "11/0"}]}'
        :type argin: str
        """
        handler = self.get_command_object("AddPortsToPTP")
        result_code, message = handler(argin)
        return [result_code], [message]

    class AddPortsToPTPCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["PTPMulti"]
            device.logger.info("AddPortsToPTP request {}".format(routes))
            ports_to_add = []
            if not routes:
                return (
                    ResultCode.FAILED,
                    "No PTP ports number given for the multicasting of PTP",
                )
            for route in routes:
                device.logger.info(
                    "Adding Port To SDP network {}".format(route)
                )
                ports_to_add.append(front_panel_regex.match(route["port"]))
            device.connector.configure_multicast_for_PTP(ports_to_add)
            message = "Ports_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more Alveo ports connected to the PTP network ",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddPTPClockPort(self, argin):
        """
        :param argin: JSON String describing one or more ports connected to PTP.
            e.g. '{"PTPClock": [{"port": "1/0"}]}'
        :type argin: str
        """
        handler = self.get_command_object("AddPTPClockPort")
        result_code, message = handler(argin)
        return [result_code], [message]

    class AddPTPClockPortCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["PTPClock"]
            device.logger.info("AddPTPClockPort request {}".format(routes))
            if not routes:
                return (
                    ResultCode.FAILED,
                    "No PTP ports number given for the multicasting of PTP",
                )
            for route in routes:
                device.logger.info(
                    "Adding Port To SDP network {}".format(route)
                )
                device.connector.add_ingress_to_multicast_session(
                    front_panel_regex.match(route["port"]), 1
                )  # Dedicated PTP session
            message = "PTP Clock Ports Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    # SDP downstream configuration
    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddSDPIPEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"sdp_ip": [{"src": {"ip": "192.168.1.1" }, "dst": {"port": "12/0"}}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("AddSDPIPEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class AddSDPIPEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["sdp_ip"]
            device.logger.info("AddSDPIPEntry request {}".format(routes))
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                device.connector.add_sdp_ip_table_entry(
                    route["src"]["ip"],
                    front_panel_regex.match(route["dst"]["port"]),
                )
            message = "SDP_IP_Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more psr rules to remove.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def RemoveSDPIPEntry(self, argin):
        """
        :param argin: JSON String describing one or more psr rules to remove.
            e.g. '{"sdp_ip": [{"src": {"ip": "192.168.1.1" } }]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("RemoveSDPIPEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class RemoveSDPIPEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["sdp_ip"]
            device.logger.info("RemoveSDP_IPEntry request {}".format(routes))
            for route in routes:
                device.logger.info("Removing route to IP {}".format(route))
                try:
                    device.connector.remove_sdp_ip_table_entry(
                        route["src"]["ip"],
                    )
                except Exception:
                    return ResultCode.FAILED, "failed to remove SDP_IP Entry"
            message = "SDP_IP_Routes_Removed"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def UpdateSDPIPEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"sdp_ip": [{"src": {"ip": "192.168.1.1" }, "dst": {"port": "12/0"}}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("UpdateSDPIPEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class UpdateSDPIPEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["sdp_ip"]
            device.logger.info("UpdateSDP_IPEntry request {}".format(routes))
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                device.connector.update_sdp_ip_table_entry(
                    route["src"]["ip"],
                    front_panel_regex.match(route["dst"]["port"]),
                )
            message = "SDP_IP_Routes_Updated"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more routing rules to delete."
        "If a route is partially specified, all matching routes will be deleted.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def ClearSDPIPTable(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to delete.
            If a route is partially specified, all matching routes will be deleted.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("ClearSDPIPTable")
        result_code, message = handler(argin)
        return [result_code], [message]

    class ClearSDPIPTableCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            device.logger.info("Deleting all rules in SDP IP")

            device.connector.clear_sdp_ip_table()
            message = "SDP_IP_Routes_Deleted"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    # SDP downstream configuration
    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddSDPMACEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"sdp_mac": [{"src": {"ip": "192.168.1.1" }, "dst": {"mac": "aa:bb:cc:dd:ee:ff"}}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("AddSDPMACEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class AddSDPMACEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["sdp_mac"]
            device.logger.info("Add SDP MAC Entry request {}".format(routes))
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                device.connector.add_sdp_mac_table_entry(
                    route["src"]["ip"],
                    route["dst"]["mac"],
                )
            message = "PSR_Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more psr rules to remove.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def RemoveSDPMACEntry(self, argin):
        """
        :param argin: JSON String describing one or more psr rules to remove.
            e.g. '{"sdp_mac": [{"src": {"ip": "192.168.1.1" }}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("RemoveSDPMACEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class RemoveSDPMACEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["sdp_ip"]
            device.logger.info(
                "Remove SDP MAC Entry request {}".format(routes)
            )
            for route in routes:
                device.logger.info("Removing route to IP {}".format(route))
                try:
                    device.connector.remove_sdp_mac_table_entry(
                        route["src"]["ip"],
                    )
                except Exception:
                    return ResultCode.FAILED, "failed to remove SDP MAC Entry"
            message = "PSR_Routes_Removed"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more spead rules to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def UpdateSDPMACEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"sdp_mac": [{"src": {"ip": "192.168.1.1" }, "dst": {"mac": "aa:bb:cc:dd:ee:ff"}}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("UpdateSDPMACEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class UpdateSDPMACEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["sdp_mac"]
            device.logger.info(
                "Update SDP MAC Entry request {}".format(routes)
            )
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                device.connector.update_sdp_mac_table_entry(
                    route["src"]["ip"],
                    route["dst"]["mac"],
                )
            message = "Routes_Added"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more routing rules to delete."
        "If a route is partially specified, all matching routes will be deleted.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def ClearSDPMACTable(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to delete.
            If a route is partially specified, all matching routes will be deleted.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("ClearSDPMACTable")
        result_code, message = handler(argin)
        return [result_code], [message]

    class ClearSDPMACTableCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            device.logger.info("Deleting all rules in SDP MAC")

            device.connector.clear_sdp_mac_table()
            message = "Routes_Deleted"
            device.logger.info(message)
            # return [[result_code], [message]]
            return ResultCode.OK, message

    # Substation  configuration
    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more Substation rules to drop in the traffic.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddSubStationEntry(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to add.
            e.g. '{"substation": [{"id": 1}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("AddSubStationEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class AddSubStationEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["substation"]
            device.logger.info(
                "Add Sub Station Entry request {}".format(routes)
            )
            for route in routes:
                device.logger.info("Adding route {}".format(route))
                device.connector.add_sub_station_entry(int(route["id"]))
            message = "Sub_Station_Routes_Added"
            device.logger.info(message)
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more substation rules to stop dropping.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def RemoveSubStationEntry(self, argin):
        """
        :param argin: JSON String describing one or more substation to stop dropping.
            e.g. '{"substation": [{"id": 1}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("RemoveSubStationEntry")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class RemoveSubStationEntryCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            routes = json.loads(argin)["substation"]
            device.logger.info(
                "Remove SubStation Entry request {}".format(routes)
            )
            for route in routes:
                device.logger.info(
                    "Removing route to Substation {}".format(route)
                )
                try:
                    device.connector.remove_sub_station_entry(
                        int(route["id"]),
                    )
                except Exception:
                    return (
                        ResultCode.FAILED,
                        "failed to remove Sub Station Entry",
                    )
            message = "Sub_Station_Routes_Removed"
            device.logger.info(message)
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more routing rules to delete."
        "If a route is partially specified, all matching routes will be deleted.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def ClearSubStationTable(self, argin):
        """
        :param argin: JSON String describing one or more routing rules to delete.
            If a route is partially specified, all matching routes will be deleted.
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("ClearSubStationTable")
        result_code, message = handler(argin)
        return [result_code], [message]

    class ClearSubStationTableCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device

            device.logger.info("Deleting all rules in SubStation")

            device.connector.clear_sub_station_table()
            message = "Routes_Deleted"
            device.logger.info(message)
            return ResultCode.OK, message

    @command(
        dtype_in="DevString",
        doc_in="JSON String describing one or more port mapping to add.",
        dtype_out="DevVarLongStringArray",
        doc_out="Information-only string",
    )
    def AddPortMapping(self, argin):
        """
        :param argin: JSON String describing one or more port mapping to add
            e.g. '{"port_mapping": [{"board_num": 1, "port_num": 1, "lane_num": 0}]}'
        :type argin: str
        :return: None
        """
        handler = self.get_command_object("AddPortMapping")
        result_code, message = handler(argin)
        return [result_code], [message]

    #
    class AddPortMappingCommand(FastCommand):
        def __init__(self, tango_device, logger):
            super().__init__(logger)
            self._tango_device = tango_device

        def do(self, argin):
            device = self._tango_device
            routes = json.loads(argin)["port_mapping"]
            device.logger.info("Add Port Mapping request {}".format(routes))
            for route in routes:
                device.logger.info("Add Port Mapping {}".format(route))
                try:
                    device.connector.add_port_mapping(
                        int(route["board_num"]),
                        int(route["port_num"]),
                        int(route["lane_num"]),
                    )
                except Exception:
                    return (
                        ResultCode.FAILED,
                        f"failed to Add Port Mapping for route {route}",
                    )
            message = "port_mapping_added"
            device.logger.info(message)
            return ResultCode.OK, message


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the LowCbfConnector module."""
    return run((LowCbfConnector,), args=args, **kwargs)


if __name__ == "__main__":
    main()
