# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

from ska_low_cbf_net.connector_protocol import Protocol


class Psr(Protocol):
    def __init__(self, target, gc, bfrt_info):
        # Set up base class

        super(Psr, self).__init__(target, gc)

        self.table = bfrt_info.table_get("psr_table")

    def _clear(self):
        """Remove all entries"""
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            self.table.entry_del(
                self.target,
                [
                    self.table.make_key(
                        [
                            self.gc.KeyTuple(
                                "beam_number",
                                key.to_dict()["beam_number"]["value"],
                            )
                        ]
                    )
                ],
            )

    def clear(self):
        self._clear()

    def add_entry(self, match, action):
        """Add one entry.

        Keyword arguments:
            dev_port -- Dest port
            beam_number -- Beam number for PSR
        """
        frequency = match.to_bytes(2, byteorder="little")
        my_bytearray = bytearray(frequency)
        self.table.entry_add(
            self.target,
            [
                self.table.make_key(
                    [self.gc.KeyTuple("beam_number", my_bytearray)]
                )
            ],
            [
                self.table.make_data(
                    [
                        self.gc.DataTuple("dest_port", action[0]),
                        self.gc.DataTuple("destination_udp_port", action[1]),
                    ],
                    "set_egr_port_beam",
                )
            ],
        )

    def remove_entry(self, match):
        """Remove one entry"""
        frequency = match.to_bytes(2, byteorder="little")
        my_bytearray = bytearray(frequency)
        self.table.entry_del(
            self.target,
            [
                self.table.make_key(
                    [self.gc.KeyTuple("beam_number", my_bytearray)]
                )
            ],
        )

    def get_entries(self):
        """Get all forwarding entries.

        Returns:
            list of (beam_number, dev port)
        """
        entries = {}
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            # print(key.to_dict()['ingress_port']['value'])
            # print(data.to_dict()['dest_port'])
            freq_bytes = key.to_dict()["beam_number"]["value"].to_bytes(
                2, byteorder="little"
            )
            byte_array = bytearray(freq_bytes)
            # print(byte_array)
            beam = int.from_bytes(byte_array, byteorder="big")
            entries[beam] = {}
            entries[beam]["dest_port"] = data.to_dict()["dest_port"]
            entries[beam]["destination_udp_port"] = data.to_dict()[
                "destination_udp_port"
            ]

        return entries

    def get_counters(self):
        """Get all forwarding counters per beams.

        Returns:
            list of (match, counters (pkts, bytes))
        """
        entries = {}
        self.table.operations_execute(self.target, "SyncCounters")
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            freq_bytes = key.to_dict()["beam_number"]["value"].to_bytes(
                2, byteorder="little"
            )
            byte_array = bytearray(freq_bytes)
            # print(byte_array)
            beam = int.from_bytes(byte_array, byteorder="big")
            entries[beam] = {
                "Pkts": data.to_dict()["$COUNTER_SPEC_PKTS"],
                "Bytes": data.to_dict()["$COUNTER_SPEC_BYTES"],
            }

        return entries

    def reset_counters(self):
        """Reset SPEAD counters"""

        # Reset direct counter
        self.table.operations_execute(self.target, "SyncCounters")
        resp = self.table.entry_get(self.target, flags={"from_hw": False})

        keys = []
        values = []

        for v, k in resp:
            keys.append(k)

            v = v.to_dict()
            k = k.to_dict()

            values.append(
                self.table.make_data(
                    [
                        self.gc.DataTuple("$COUNTER_SPEC_BYTES", 0),
                        self.gc.DataTuple("$COUNTER_SPEC_PKTS", 0),
                    ],
                    v["action_name"],
                )
            )

        self.table.entry_mod(self.target, keys, values)

    def update_entry(self, match, action):
        """
        Keyword arguments:
            action -- Dest port
            match -- beam number
        """
        installed_action = False
        frequency = match.to_bytes(2, byteorder="little")
        my_bytearray = bytearray(frequency)
        entries_from_switch = self.table.entry_get(self.target)
        for data, key in entries_from_switch:
            if key.to_dict()["beam_number"]["value"] == int.from_bytes(
                my_bytearray, byteorder="big"
            ):
                installed_action = True
        if installed_action:
            self.table.entry_mod(
                self.target,
                [
                    self.table.make_key(
                        [self.gc.KeyTuple("beam_number", my_bytearray)]
                    )
                ],
                [
                    self.table.make_data(
                        [
                            self.gc.DataTuple("dest_port", action[0]),
                            self.gc.DataTuple(
                                "destination_udp_port", action[1]
                            ),
                        ],
                        "set_egr_port_beam",
                    )
                ],
            )
        else:
            self.add_entry(match, action)
