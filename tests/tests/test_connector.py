"""
Tests for Connector class
"""
from dataclasses import dataclass
from time import sleep
from unittest.mock import Mock, patch

import pytest

from ska_low_cbf_conn.spead_connector import SpeadConnector


class TestSpeadConnector:
    # I haven't written tests for all the tables,
    # because we are likely to change P4 API soon
    @pytest.mark.skip(
        "'Connector' has no attribute '_update_spead_loss_count'"
    )
    def test_update_spead_loss(self):
        ce = Mock()
        sh = Mock()

        @dataclass
        class Entry:
            index: int
            packet_count: int
            byte_count: int

        counter_data = [
            Entry(0, 1, 1000),
            Entry(1, 0, 0),
            Entry(2, 2, 2000),
            Entry(3, 333, 333333),
        ]
        channel_set = {0, 1, 2}
        ce.read.return_value = counter_data
        sh.CounterEntry.return_value = ce
        connector = Mock()
        connector.monitor_channels = channel_set
        connector._spead_bytes_lost = {}
        connector._spead_packets_lost = {}

        SpeadConnector._update_spead_loss_count(connector, sh)

        sh.CounterEntry.assert_called_once_with("counter_spead_losses")
        assert connector._spead_bytes_lost == {
            _.index: _.byte_count
            for _ in counter_data
            if _.index in channel_set
        }
        assert connector._spead_packets_lost == {
            _.index: _.packet_count
            for _ in counter_data
            if _.index in channel_set
        }
