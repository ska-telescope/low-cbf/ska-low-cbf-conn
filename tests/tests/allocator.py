# -*- coding: utf-8 -*-
#
# This file is part of the ska_low_cbf_net project
#
# Copyright (c) 2021 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.

import re


class Allocator:
    def __init__(self, filename):
        self.list_of_beamformer = []
        self.list_of_cbf = []
        self.port_to_veth = {
            "1/0": "veth0",
            "1/1": "veth2",
            "1/2": "veth4",
            "1/3": "veth6",
            "2/0": "veth8",
            "2/1": "veth10",
            "2/2": "veth12",
            "2/3": "veth14",
        }
        self.list_of_available_ports = [
            "1/0",
            "1/1",
            "1/3",
            "2/0",
            "2/1",
            "2/2",
            "2/3",
        ]
        self.config_port_to_available = {}
        self.set_spead_entries(filename)

    def set_spead_entries(self, allocator_config_file):
        """Add Spead Table entries. Based on a file from the allocator
        Keyword arguments:
            line -- allocator file name
        This function create a entry and take two arguments, first the frequency then, beam, number, and then egress port.
        """
        file_name = allocator_config_file.strip()

        file1 = open(file_name, "r")
        Lines = file1.readlines()
        sub = "sub"
        star = "***"
        for line in Lines:
            configuration = []
            entries = re.split("\W+", line.strip())
            if len(entries[0].strip()) == 1:
                if len(entries) == 5:
                    configuration.append(int(entries[0].strip()))
                    configuration.append(int(entries[1].strip()))
                    configuration.append(int(entries[2].strip()))
                    configuration.append(
                        self.get_available_port_from_configure(
                            str(entries[3].strip())
                        )
                    )
                    self.list_of_beamformer.append(configuration)

                elif len(entries) == 6:
                    configuration.append(int(entries[0].strip()))
                    configuration.append(int(entries[1].strip()))
                    configuration.append(int(entries[2].strip()))
                    configuration.append(
                        self.get_available_port_from_configure(
                            str(entries[3].strip())
                        )
                    )
                    configuration.append(
                        self.get_available_port_from_configure(
                            str(entries[4].strip())
                        )
                    )
                    self.list_of_cbf.append(configuration)
        print("Beamformer: ")
        print(self.list_of_beamformer)
        print("CBF: ")
        print(self.list_of_cbf)
        # ports = entries[3 : (len(entries) - 1)]

    def get_available_port_from_configure(self, port):
        if port in self.config_port_to_available:
            return self.config_port_to_available[port]
        else:
            self.config_port_to_available[port] = self.list_of_available_ports[
                len(self.config_port_to_available)
            ]
            return self.config_port_to_available[port]

    def get_list_of_beamformer(self):
        return self.list_of_beamformer

    def get_list_of_cbf(self):
        return self.list_of_cbf

    def get_veth_from_port(self, port):
        return self.port_to_veth[port]
