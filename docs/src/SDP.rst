******************
Interfacing with SDP in AA0.5
******************


In this document, we aim at describing how, in AA0.5, the P4 switch will:

* be connected to SDP;
* provide ARP requests to SDP;
* interpret ARP answers from SDP;
* be connected to the 6 Alveo;
* route SPEAD traffic to SDP;
* be configured to perform SPEAD/SDP routing.

Overall we are interested in describing the operation of the P4 switch in the scenario depicted below.

.. image:: diagrams/p4_sdp_05.png
  :width: 400
  :alt: SPEAD header

Hardware
################

In this configuration the P4 switch is using 7 of the 100G ports as follows:

* 6 ports connected to the Alveo server using 100G DAC cables
* 1 physical ports connected to SDP using 100G QSFP LR.

In addition, the P4 switch is using 2 10G ports, one for the Management&Control and one for connection to the PTP switch.

Using ARP to help SDP topology
###############

For the downstream integration with SDP, Low.CBF needs to before sending any data to discover the MAC address associated to a given SDP node as shown in the diagram below from `ECP-210085 - CBF, PSS, PST to obtain the destination MAC address using ARP <https://confluence.skatelescope.org/display/CMI/ECP-210085+-+CBF%2C+PSS%2C+PST+to+obtain+the+destination+MAC+address+using+ARP>`_.

.. image:: diagrams/arp_reply.png
  :width: 400
  :alt: SPEAD header

As a result, we foresee that the ARP integration for communication to SDP could be achieved as follows:

* Low CBF receives Correlator configuration alongside destination IP addresses for the various outputs (from TM subarray LMC via CSP&CBF subarray LMC)
* Low.CBF allocator configures Alveos and P4 switch accordingly
* P4 switch configuration consists in routing port and/or destination IP address that match the various configuration. In more details:
    * Configuration is done via Tango which in turn configures the SKA connector
    * SKA connector would:
        * If Intermediate switch, just apply configuration as it knows destination port (i.e. I/O switch)
        * If I/O switch, the procedure would be as follows:
            * Connector passes IP address to an internal ARP resolver thread
            * Thread sends ARP queries via local Linux interface connected to port 33/2 on the switch (and listen on the same interface for responses)
            * These queries are broadcasted to all downstream ports to SDP
            * P4 switch should receive response on a single port (MAC address green arrow in the diagram above) and as a result it will generate a packet containing <IP@, Mac@, received_port> information to port 65/0
            * Using this information SKA connector configures P4 table associated to this configuration
            * Switch would modify packets on the fly  to have correct addresses during sending of the visibility data
            * In the background ARP resolver would continue to send query to mitigate potential reconfiguration downstream and help SDP maintaining their topology
            * If changes are detected the SKA connector will re-configure table automatically
* Association table <IP@, Mac@, port> would be accessible via Connector Tango Davice at anytime

In regards to the data stream going to PSS and PST, it is assumed at the moment that the integration via ARP would follow a similar procedure as for SDP.


Tango integration
================

This interaction through the tango device is realised through 2 methods and 1 attribute:

* AddPortsToSDPARP, which add a list of physical ports to the ARP resolution process. The argument
for this method is a json string of the form:

.. code-block:: json

    '{"SDP": [{"port": "10/0"}]}'

* AddIPToResolve, which add a list of IP address to resolve. This method take a json
string of the following form as an argument:

.. code-block:: json

    '{"IP": [{"address": "192.168.1.1"}]}'

* arp_replies attrribute returns a json string maching IP Addresses to both
MAC address and P4 port number

LFAA routing for SDP
###############

Outgoing traffic
================

In the context of AA0.5, Low CBF correlator will generate  0.2Gbps of raw data. Computation for the various data rate can
be found `here <https://docs.google.com/spreadsheets/d/1Qza66EnFgSQyeJwhoM_vxqVzMsNe8bHSmg6CI6Q3nSk/edit#gid=978340330>`_.
This raw data is encapsulated in `SPEAD <https://casper.astro.berkeley.edu/astrobaki/images/9/93/SPEADsignedRelease.pdf>`_
packet.

P4 routing
================

Prior to generate traffic, Low CBF would have received a list of IP addresses and UDP port numbers for the visibility
traffic. In this context, the programmable switch just needs to perform 2 actions upon reception of SPEAD-SDP traffic:

* IP forwarding based on destination IP address

* MAC address substitution based on the ARP resolution mentioned above.


Tango methods
===============

* AddSDPIPEntry: adding an IP forwarding table entry sending all packets from IP address i to Port p

.. code-block:: json

    '{"sdp_ip": [{"src": {"ip": "192.168.1.1" }, "dst": {"port": "12/0"}}]}'


* RemoveSDPIPEntry: removing an IP forwarding table entry

.. code-block::

    '{"sdp_ip": [{"src": {"ip": "192.168.1.1" }}]}'

* UpdateSDPIPEntry: updating an IP forwarding table entry sending all packets from IP address i to Port p

.. code-block:: json

    '{"sdp_ip": [{"src": {"ip": "192.168.1.1" }, "dst": {"port": "12/0"}}]}'

* ClearSDPIPTable: emptying the IP forwarding table

* sdpIpRoutingTable: return the current instance of the IP forwarding table from the switch

* AddSDPMACEntry: adding an Change Mac Address table entry changing the destination MAC address of all packets with
IP address i to MAC address m

.. code-block:: json

    '{"sdp_mac": [{"src": {"ip": "192.168.1.1" }, "dst": {"mac": "aa:bb:cc:dd:ee:ff"}}]}'

* RemoveSDPMACEntry: removing an Change Mac Address table entry

.. code-block:: json

    '{"sdp_ip": [{"src": {"ip": "192.168.1.1" }}]}'

* UpdateSDPMACEntry: updating an Change Mac Address table entry changing the destination MAC address of all packets with
IP address i to MAC address m

.. code-block:: json

    '{"sdp_mac": [{"src": {"ip": "192.168.1.1" }, "dst": {"mac": "aa:bb:cc:dd:ee:ff"}}]}'

* ClearSDPMACTable: emptying the an Change Mac Address table

* sdpMacRoutingTable: return the current instance of the Change Mac Address table from the switch