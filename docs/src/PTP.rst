*********************************
Interfacing with PTP in AA0.5
*********************************

WARNING: This is a work in progress

In this document, we aim at describing how, in AA0.5, the P4 switch will:

* be connected to a PTP boundary clock;
* multicast PTP traffic from the clock to all Alveo cards
* send back PTP traffic from the Alveo to the PTP clock



Hardware
################

In this configuration the P4 switch will use 7 of its 100G ports as follows:

* 6 switch ports connected to 6 Alveo cards using 100G DAC cables
* 1 switch ports connected to PTP using breakdown cable to provide 10G on one of the switch port lane.

In addition to the 100G ports, one of the P4 switch's 10G management ports will be used for the Management&Control.


PSR routing
################

In/Out-going traffic
================

In the specification of IEEE1588, all PTP packets are supposed to be broadcast to all ports. This includes
packets from the master clock as one would expect, but also all message from the client/slave clock (Alveo
cards for us). However, the Alveo messages clearly do not need to be broadcasted to all other Alveos as they
would be directly discarded. Therefore in the P4 switch we make sure that those messages are only sent to the
master clock. Furthermore, the P4 switch is only broadcasting messages from the master to know Alveo card and
not all active ports. This behaviour and the associated commands to configure all that through Tango are shown
in the diagram below

.. image:: diagrams/PTP_control.png
  :width: 400
  :alt: PTP control and traffic

Tango methods
==============

* AddPTPEntry that takes JSON String describing one or more PTP routing rules to add.

.. code-block:: json

    '{"ptp": [{"src": {"port": "1/0"}, "dst": {"port": "2/0"}}]}'

* UpdatePTPEntry that takes JSON String describing one or more PTP routing rules to add.

.. code-block:: json

    '{"ptp": [{"src": {"port": "1/0"}, "dst": {"port": "2/0"}}]}'

* ClearPTPTable that removes all rules related to Alveo to clock table

* AddPortsToPTP that takes JSON String describing one or more ports connected to PTP multicast session

.. code-block:: json

    '{"PTPMulti": [{"port": "10/0"}, {"port": "11/0"}]}'

* AddPTPClockPort that take JSON String describing one or more ports connected to PTP. Note that more that one PTP
is possible despite not been useful

.. code-block:: json

    '{"PTPClock": [{"port": "1/0"}]}'
